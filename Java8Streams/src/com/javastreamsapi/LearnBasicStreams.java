package com.javastreamsapi;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LearnBasicStreams {

	public static void main(String[] args) {
		Stream<Integer> integerStream=Stream.of(1,2,6,8,9);
		Integer[] intArray=integerStream.toArray(Integer[]::new);
		System.out.println(Arrays.toString(intArray));

		Integer[] array=new Integer[]{7,9,6,8};

		Stream<Integer> integerStream2=Stream.of(array);
		Integer[] intArray2=integerStream2.toArray(Integer[]::new);//Integer[]::new-------lambda expression
		System.out.println(Arrays.toString(intArray2));
		//since stream has already operated
		integerStream2=Stream.of(array);
		//for filtering streams
		integerStream2.filter(p -> p >= 8)
		.forEach(c -> System.out.println("c>=8 = "+c));
		integerStream2=Stream.of(array);
		integerStream2.filter(p -> p > 2)
		.forEach(c -> System.out.println("C > 2 ="+c));


		//mapping streams and then adding
		integerStream2=Stream.of(array);
		System.out.println(integerStream2.mapToInt(i -> i).sum());

		//converting stream to collections list
		integerStream2=Stream.of(array);
		List<Integer> intList=integerStream2.collect(Collectors.toList());
		System.out.println(intList);

		//map example
		integerStream2=Stream.of(array);
		System.out.println("map example= "+integerStream2
				.map(s -> {return s.intValue();})
		.collect(Collectors.toList()));

		//sorted example
		integerStream2=Stream.of(array);
		System.out.println("Sorted example= "+
				integerStream2.sorted(Comparator.naturalOrder()).collect(Collectors.toList()));

		integerStream2=Stream.of(array);
		System.out.println("Reverse Sorted example= "+
				integerStream2.sorted(Comparator.reverseOrder())
		.collect(Collectors.toList()));

		//stream of List
		//flat map example
		Stream<List<String>> listStrings=Stream.of(
				Arrays.asList("one", "two"),
				Arrays.asList("three", "four"),
				Arrays.asList("five")
				);
		Stream<String> flatStreamStrings=listStrings.flatMap((strList) -> strList.stream());
		System.out.println("map example= "+flatStreamStrings
				.map(s -> {return s;})
		.collect(Collectors.toList()));
		
		//reduce example
		listStrings=Stream.of(
				Arrays.asList("one", "two"),
				Arrays.asList("three", "four"),
				Arrays.asList("five")
				);
		flatStreamStrings=listStrings.flatMap(strList -> strList.stream());
		Optional<String> strOptional=flatStreamStrings.reduce((i,j) -> {return i+j;});
		if(strOptional.isPresent()){
			System.out.println(strOptional.get());
		}
		
		//to find sum of all nos. by reduce
		integerStream2=Stream.of(array);
		Optional<Integer> intOptional=integerStream2.reduce((i,j) -> {return i+j;});
		if(intOptional.isPresent()){
			System.out.println(intOptional.get());
		}
		
		//count to count the items in a stream
		integerStream2=Stream.of(array);
		System.out.println("no. of elements in stream = "+ integerStream2.count());
		
		listStrings=Stream.of(
				Arrays.asList("one", "two"),
				Arrays.asList("three", "four"),
				Arrays.asList("five")
				);
		System.out.println("no. of elements in list strings stream = "+ listStrings.count());
		
		listStrings=Stream.of(
				Arrays.asList("one", "two"),
				Arrays.asList("three", "four"),
				Arrays.asList("five")
				);
		flatStreamStrings=listStrings.flatMap(strList -> strList.stream());
		System.out.println("no. of elements in stream = "+ flatStreamStrings.count());
		
		
		//match example
		integerStream2=Stream.of(1,2,3,4,5,6,7);
		System.out.println("this stream contains integer=5 is "+integerStream2.anyMatch(p -> p==5));
		
		integerStream2=Stream.of(1,2,3,4,5,6,7);
		System.out.println("this stream contains integer<5 is "+integerStream2.allMatch(p -> p<5));
		
		Stream<String> stringStream=Stream.of("abc", "suji","pooja","ced","hugh");
		System.out.println("Stirng stream contains ''suji'' is "+stringStream.anyMatch(p -> p.equals("suji")));
		
		integerStream2=Stream.of(1,2,3,4,5,6,7);
		System.out.println("this stream does not contain integer=11 is "+integerStream2.noneMatch(p -> p==11));
		
		
		//find first example
		stringStream=Stream.of("abc", "suji","pooja","sean","hugh");
		strOptional=stringStream.filter( p -> p.startsWith("s")).findFirst();
		if(strOptional.isPresent()){
			System.out.println("first string starting with 's' = "+strOptional.get());
		}
	}

}
