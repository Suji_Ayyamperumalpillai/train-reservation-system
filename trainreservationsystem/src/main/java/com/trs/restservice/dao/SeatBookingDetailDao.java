package com.trs.restservice.dao;

import java.util.Date;
import java.util.List;

import com.trs.restservice.models.BerthDetail;
import com.trs.restservice.models.SeatBookingDetail;

public interface SeatBookingDetailDao {

	List<SeatBookingDetail> list();

	Long getTotalAvailableSheetOfAc(int trainId, Date bookingDate);

	int getTotalAvailableSheetOfSleeper(int trainId, Date bookingDate);
	
	List<SeatBookingDetail> getTotalSheetOfBirthPreference(String birthName, int trainId, Date bookingDate);
	
	List<SeatBookingDetail> getStatusOfBerthId(int sbId);

	void postStatusOfBerthId(SeatBookingDetail seatBookingDetail);

	List<SeatBookingDetail> getStatusOfAvailableBerth(int trainId, int coachId, Date bookingDate);

	void changeStatusOfBerth(SeatBookingDetail seatBookingDetail);

	

}
