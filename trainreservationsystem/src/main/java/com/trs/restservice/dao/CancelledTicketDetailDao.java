package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.CancelledTicketDetail;

public interface CancelledTicketDetailDao {

	void saveCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail);

	CancelledTicketDetail getCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail);
	
	List<CancelledTicketDetail> getCancelledTicketsList(int udId);

}
