package com.trs.restservice.dao;

import com.trs.restservice.models.Country;

public class SaveDataManually extends AbstractDao{
	
	public void saveCountry(){
		
		getSession().beginTransaction();
		
		Country country=new Country();
		country.setCountryName("test");
		
		getSession().save(country);
		getSession().getTransaction().commit();
		
		getSession().close();
	}
	public static void main(String[] args) {
		SaveDataManually object=new SaveDataManually();
		object.saveCountry();
	}

}
