package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.State;

@Repository
public class StateDaoImpl extends AbstractDao implements StateDao {

	@Override
	public State getCountry(int stateId) {
		State state=getSession().get(State.class, stateId);
		return state;
	}

	@Override
	public List<State> list() {
		Query qry= getSession().createQuery("from State");
		List<State> states=qry.list();
		return states;
	}

	@Override
	public List<State> listByCountryId(int countryId) {
		Query qry= getSession().createQuery("from State s WHERE s.country.countryId = "+countryId);
		List<State> states=qry.list();
		return states;
	}

}
