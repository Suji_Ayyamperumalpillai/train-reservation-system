package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.State;

public interface StateDao {

	public State getCountry(int stateId);
	
	public List<State> list();
	
	public List<State> listByCountryId(int countryId);
}
