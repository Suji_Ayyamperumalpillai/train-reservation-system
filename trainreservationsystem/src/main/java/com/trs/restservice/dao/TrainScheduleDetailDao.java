package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.TrainScheduleDetail;

public interface TrainScheduleDetailDao {

	List<TrainScheduleDetail> list();

	List<TrainScheduleDetail> listByStation(String stationCode);

	List<TrainScheduleDetail> listByTrainId(int trainId);

}
