package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.SecurityQuestion;

public interface SecurityQuestionDao{

	public SecurityQuestion getSecurityQuestion(int sqId);

	public List<SecurityQuestion> list();

	public List<SecurityQuestion> listOfActiveSecurityQuestions();
}
