package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.TrainDetail;

public interface TrainDetailDao {

	List<TrainDetail> list();

	TrainDetail listByTrainId(int trainId);

}
