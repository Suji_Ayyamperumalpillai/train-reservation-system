package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.TrainDetail;

@Repository
public class TrainDetailDaoImpl extends AbstractDao implements TrainDetailDao{

	public List<TrainDetail> list() {
		Query qry=getSession().createQuery("from TrainDetail");
		List<TrainDetail> trainList=qry.list();
		return trainList;
	}

	public TrainDetail listByTrainId(int trainId) {
		TrainDetail trainDetail = getSession().get(TrainDetail.class, trainId);
		return trainDetail;
	}

}
