package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.TrsContactDetail;

@Repository
public class TrsContactDetailDaoImpl extends AbstractDao implements TrsContactDetailDao{

	@Override
	public void saveContactsDetails(TrsContactDetail trsContactDetail) {
		getSession().save(trsContactDetail);
	}

	@Override
	public TrsContactDetail getTrsContactDetailDao(int trscId) {
		TrsContactDetail contactDetail=getSession().get(TrsContactDetail.class, trscId);
		return contactDetail;
	}

	@Override
	public List<TrsContactDetail> list() {
		Query qry=getSession().createQuery("from TrsContactDetail");
		List<TrsContactDetail> list=qry.list();
		return list;
	}

	

}
