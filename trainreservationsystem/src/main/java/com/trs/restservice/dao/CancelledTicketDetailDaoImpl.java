package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.CancelledTicketDetail;

@Repository
public class CancelledTicketDetailDaoImpl extends AbstractDao implements CancelledTicketDetailDao {

	@Override
	public void saveCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail) {
		getSession().save(cancelledTicketDetail);
		
	}
	
	@Override
	public List<CancelledTicketDetail> getCancelledTicketsList(int udId) {
	Query qry=getSession().createQuery("from CancelledTicketDetail ct WHERE ct.transactionDetail.userDetail.udId= :udId");
	qry.setParameter("udId", udId);
	List<CancelledTicketDetail> cancelledTicketDetail=qry.list();	
	return cancelledTicketDetail;
	}
	
	@Override
	public CancelledTicketDetail getCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail) {
		Query qry=getSession().createQuery("from CancelledTicketDetail ct WHERE ct.transactionDetail.tnsId= :tnsId");
		qry.setParameter("tnsId", cancelledTicketDetail.getTransactionDetail().getTnsId());
		
		return (CancelledTicketDetail) qry.uniqueResult();
	}


}
