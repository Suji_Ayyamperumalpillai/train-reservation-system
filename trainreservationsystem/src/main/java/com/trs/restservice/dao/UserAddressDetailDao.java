package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.UserAddressDetail;

public interface UserAddressDetailDao {

	public void saveUserAddress(UserAddressDetail addressDetail);
	
	public void updateUserAddress(UserAddressDetail addressDetail);
	
	public void deleteUserAddress(int uadId);
	
	public UserAddressDetail getUserAddress(int uadId);
	
	public List<UserAddressDetail> list();
}
