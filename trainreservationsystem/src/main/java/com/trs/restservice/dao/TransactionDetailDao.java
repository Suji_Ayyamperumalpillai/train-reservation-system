package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.TransactionDetail;

public interface TransactionDetailDao {
	
	public void saveTransactionDetail(TransactionDetail transactionDetail);
	
	public void deleteTransactionDetail(int tnsId);
	
	public TransactionDetail getTransactionDetail(int tnsId);

	public List<TransactionDetail> getTransactionDetailByUserId(int userId);

	public void changeReservationStatus(TransactionDetail transactionDetail);

	List<TransactionDetail> getBookedTicketsDetailByUserId(int udId);

	TransactionDetail getLastTransactionDetailByUserId(int udId);
	
}

