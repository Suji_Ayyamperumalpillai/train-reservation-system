package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.UserDetail;

@Repository
public class UserDetailDaoImpl extends AbstractDao implements UserDetailDao {

	@Override
	public void saveUserDetails(UserDetail userDetail) {
		getSession().save(userDetail);
	}

	@Override
	public void updateUserDetail(UserDetail userDetail) {
		getSession().update(userDetail);
	}

	@Override
	public void deleteUserDetail(int udId) {
		Query delQry=getSession()
				.createQuery("DELETE FROM UserDetail WHERE udId = "+udId);
		delQry.executeUpdate();
	}

	@Override
	public UserDetail getUserDetail(int udId) {
		UserDetail userDetail=getSession().get(UserDetail.class, udId);
		return userDetail;
	}

	@Override
	public List<UserDetail> getList() {
		Query qry=getSession().createQuery("from UserDetail");
		List<UserDetail> list=qry.list();
		
		return list;
	} 
	@Override
	public boolean isAuthUser(UserDetail user) {
		Criteria crit = getSession().createCriteria(UserDetail.class);
		crit.setProjection(Projections.rowCount());
		crit.add( Restrictions.eq("userId", user.getUserId()));
		crit.add( Restrictions.eq("userPassword", user.getUserPassword()));
		long count = (long) crit.uniqueResult();
		if(count>0){
			return true;

		}
		return false;
	}
	@Override
	public boolean isUserExist(UserDetail user) {
		Criteria crit = getSession().createCriteria(UserDetail.class);
		crit.setProjection(Projections.rowCount());
		crit.add( Restrictions.eq("userId", user.getUserId()));
		long count = (long) crit.uniqueResult();
		if(count>0){
			return true;

		}
		return false;
	}

	@Override
	public UserDetail getUserByUserId(String userId) {
		System.out.println("userID "+userId);
		Query qry=getSession().createQuery("from UserDetail ud WHERE ud.userId = :userId");
		qry.setParameter("userId", userId);
		UserDetail userDetail=(UserDetail) qry.uniqueResult();
		return userDetail;
	} 
	
	
}
