package com.trs.restservice.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.trs.restservice.date.LastTimeOfDate;
import com.trs.restservice.models.SeatBookingDetail;


@Repository
public class SeatBookingDetailDaoImpl extends AbstractDao implements SeatBookingDetailDao {

	
	LastTimeOfDate lastTimeOfDate = new LastTimeOfDate(); ;
	
	@Override
	public List<SeatBookingDetail> list() {
		Query qry=getSession().createQuery("from SeatBookingDetail");
		List<SeatBookingDetail> seatBookingList=qry.list();
		return seatBookingList;
	}

	@Override
	public Long getTotalAvailableSheetOfAc(int trainId, Date bookingDate) {
	    
		Date endOfDay = lastTimeOfDate.getEndOfDay(bookingDate);
	    
		System.out.println(bookingDate);
		System.out.println(endOfDay);
		Criteria crit = getSession().createCriteria(SeatBookingDetail.class,"sb")
				.createAlias("sb.trainDetail","td")
				.createAlias("sb.subCoachDetail","scd")
				.createAlias("scd.coachDetail","cd")
				.add(Restrictions.eq("td.trainId", trainId))
				.add(Restrictions.between("sb.bookingForDate", bookingDate,endOfDay))
				.add(Restrictions.eq("cd.coachId", 2))
				.add(Restrictions.eq("sb.bookingStatus", "AVAILABLE"))
				.setProjection(Projections.rowCount());
		Long count = (Long)crit.uniqueResult();
		return count;
	}


	@Override
	public int getTotalAvailableSheetOfSleeper(int trainId, Date bookingDate) {
		Criteria crit = getSession().createCriteria(SeatBookingDetail.class,"sb")
				.createAlias("sb.trainDetail","td")
				.createAlias("sb.subCoachDetail","scd")
				.createAlias("scd.coachDetail","cd")
				.add(Restrictions.eq("td.trainId", trainId))
				.add(Restrictions.eq("sb.bookingForDate", bookingDate))
				.add(Restrictions.eq("cd.coachId", 1))
				.add(Restrictions.eq("sb.bookingStatus", "AVAILABLE"))
				.setProjection(Projections.rowCount());
		Integer count = (Integer)crit.uniqueResult();
		return count;
	}

	

	@Override
	public List<SeatBookingDetail> getTotalSheetOfBirthPreference(String birthName, int trainId, Date bookingDate) {
		Criteria criteria = getSession().createCriteria(SeatBookingDetail.class,"sb")
				.createAlias("sb.trainDetail","td")
				.createAlias("sb.berthDetail","bd")
				.add(Restrictions.eq("bd.berthName", birthName))
				.add(Restrictions.eq("sb.bookingForDate", bookingDate))
				.add(Restrictions.eq("td.trainId", trainId));
		List<SeatBookingDetail> totalSheetOfBirthPreferenc = criteria.list();
		return totalSheetOfBirthPreferenc;
	}


	@Override
	public List<SeatBookingDetail> getStatusOfBerthId(int sbId) {
		Criteria criteria = getSession().createCriteria(SeatBookingDetail.class,"sb")
				.add(Restrictions.eq("sb.sbId", sbId));

		List<SeatBookingDetail> statusOfBerthId = criteria.list();
		return statusOfBerthId;
	}

	@Override
	public List<SeatBookingDetail> getStatusOfAvailableBerth(int trainId, int coachId, Date bookingDate) {

		Criteria criteria = getSession().createCriteria(SeatBookingDetail.class,"sb")
				.createAlias("sb.trainDetail","td")
				.createAlias("sb.subCoachDetail","scd")
				.createAlias("scd.coachDetail","cd")
				.add(Restrictions.eq("td.trainId", trainId))
				.add(Restrictions.eq("cd.coachId", coachId))
				.add(Restrictions.eq("sb.bookingForDate",bookingDate))
				.add(Restrictions.eq("sb.bookingStatus", "AVAILABLE"));

		List<SeatBookingDetail> statusOfAvailableBerth = criteria.list();
		return statusOfAvailableBerth;
	}

	@Override
	public void postStatusOfBerthId(SeatBookingDetail seatBookingDetail) {
		getSession().update(seatBookingDetail);
	}


	@Override
	public void changeStatusOfBerth(SeatBookingDetail seatBookingDetail) {
		// TODO Auto-generated method stub

	}








}
