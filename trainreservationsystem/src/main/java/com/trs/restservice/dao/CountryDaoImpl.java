package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.Country;

@Repository
public class CountryDaoImpl extends AbstractDao implements CountryDao {

	@Override
	public Country getCountry(int countryId) {
		Country country=getSession().get(Country.class, countryId);
		return country;
	}

	@Override
	public List<Country> list() {
		Query qry=getSession().createQuery("from Country ORDER BY country_id");
		
		List<Country> countries=qry.list();
		return countries;
	}

}
