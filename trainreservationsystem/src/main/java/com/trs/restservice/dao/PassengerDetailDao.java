package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.PassengerDetail;
import com.trs.restservice.models.TransactionDetail;

public interface PassengerDetailDao {

	void savePassengerDetails(PassengerDetail passengerDetail);

	PassengerDetail getPassengerByTransactionId(int tnsId);

	List<PassengerDetail> getPessengerDetails();

}
