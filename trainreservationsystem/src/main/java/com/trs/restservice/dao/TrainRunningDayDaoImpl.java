package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.TrainRunningDay;

@Repository
public class TrainRunningDayDaoImpl extends AbstractDao implements TrainRunningDayDao {

	@Override
	public List<TrainRunningDay> list(int trainId) {
		
		
		Criteria criteria = getSession().createCriteria(TrainRunningDay.class,"tr")
			    .createAlias("tr.trainDetail","td")
			    .add(Restrictions.eq("td.trainId", trainId));
		 List<TrainRunningDay> trainRunningDay = criteria.list();
		return trainRunningDay;
	}

}
