package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.AlertAndUpdateDetail;

public interface AlertAndUpdateDetailDao {

	public void saveAlertsDetails(AlertAndUpdateDetail alertAndUpdateDetail);
	
	public AlertAndUpdateDetail getAlertsDetailDao(int audId);
	
	public List<AlertAndUpdateDetail> list();

	public List<AlertAndUpdateDetail> listOfActiveUpdates();
}
