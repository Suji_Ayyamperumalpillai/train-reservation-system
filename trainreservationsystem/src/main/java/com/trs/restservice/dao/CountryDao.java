package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.Country;

public interface CountryDao {

	public Country getCountry(int countryId);
	
	public List<Country> list();
}
