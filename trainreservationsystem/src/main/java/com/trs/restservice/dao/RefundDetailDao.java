package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.RefundDetail;

public interface RefundDetailDao {

	public void saveRefundDetail(RefundDetail refundDetail);
	
	public List<RefundDetail> getRefundDetailList(int udId);
}
