package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.City;
import com.trs.restservice.models.TrainRunningDay;
import com.trs.restservice.models.TrainScheduleDetail;

@Repository
public class TrainScheduleDetailDaoImpl extends AbstractDao implements TrainScheduleDetailDao {

	@Override
	public List<TrainScheduleDetail> list() {
		Query qry=getSession().createQuery("from TrainScheduleDetail");
		List<TrainScheduleDetail> stationList=qry.list();
		return stationList;
	}

	@Override
	public List<TrainScheduleDetail> listByStation(String stationCode) {
		
		Criteria criteria = getSession().createCriteria(TrainScheduleDetail.class,"tr")
			    .add(Restrictions.eq("tr.stationCode", stationCode));
		 List<TrainScheduleDetail> trainListByStation = criteria.list();

		return trainListByStation;
	}

	@Override
	public List<TrainScheduleDetail> listByTrainId(int trainId) {
		
		Criteria criteria = getSession().createCriteria(TrainScheduleDetail.class,"tr")
				.createAlias("tr.trainDetail","td")
			    .add(Restrictions.eq("td.trainId", trainId));
		 List<TrainScheduleDetail> trainListByTrainId = criteria.list();

		return trainListByTrainId;
	}

}
