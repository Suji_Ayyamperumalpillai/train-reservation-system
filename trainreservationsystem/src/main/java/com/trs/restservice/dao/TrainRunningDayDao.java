package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.TrainRunningDay;

public interface TrainRunningDayDao {

	List<TrainRunningDay> list(int trainId);

}
