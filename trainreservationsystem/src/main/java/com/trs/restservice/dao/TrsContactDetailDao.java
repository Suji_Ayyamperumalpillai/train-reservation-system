package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.TrsContactDetail;

public interface TrsContactDetailDao {

	public void saveContactsDetails(TrsContactDetail trsContactDetail );
	
	public TrsContactDetail getTrsContactDetailDao(int trscId);
	
	public List<TrsContactDetail> list();
}
