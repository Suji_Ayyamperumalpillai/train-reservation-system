package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.City;

@Repository
public class CityDaoImpl extends AbstractDao implements CityDao {

	@Override
	public City getCity(int cityId) {
		City city=getSession().get(City.class, cityId);
		return city;
	}

	@Override
	public List<City> list() {
		Query qry=getSession().createQuery("from City");
		List<City> cityList=qry.list();
		
		return cityList;
	}

	@Override
	public List<City> listByStateId(int stateId) {
		Query qry=getSession().createQuery("from City c WHERE c.state.stateId = "+stateId);
		List<City> cityList=qry.list();
		
		return cityList;
	}

}
