package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.PassengerDetail;
import com.trs.restservice.models.SeatBookingDetail;
import com.trs.restservice.models.TransactionDetail;

@Repository
public class PassengerDetailDaoImpl extends AbstractDao implements PassengerDetailDao {

	@Override
	public List<PassengerDetail> getPessengerDetails() {
		Query qry=getSession().createQuery("from PassengerDetail");
		List<PassengerDetail> pessengerDetails=qry.list();
		return pessengerDetails;
		
	}
	
	@Override
	public void savePassengerDetails(PassengerDetail passengerDetail) {
		getSession().save(passengerDetail);
	}

	@Override
	public PassengerDetail getPassengerByTransactionId(int tnsId) {

		Criteria criteria = getSession().createCriteria(PassengerDetail.class,"ps")
				.createAlias("ps.transactionDetail","ts")
                .add(Restrictions.eq("ts.tnsId", tnsId));

		PassengerDetail passengerDetail = (PassengerDetail) criteria.uniqueResult();

		return passengerDetail;
	}

	

}
