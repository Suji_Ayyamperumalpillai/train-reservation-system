package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.SecurityQuestion;

@Repository
public class SecurityQuestionDaoImpl  extends AbstractDao  implements SecurityQuestionDao {

	@Override
	public SecurityQuestion getSecurityQuestion(int sqId) {
		SecurityQuestion securityQuestion=getSession().get(SecurityQuestion.class, sqId);
		return securityQuestion;
	}

	@Override
	public List<SecurityQuestion> list() {
		Query qry=getSession().createQuery("FROM SecurityQuestion");
		List<SecurityQuestion> questions=qry.list();
		return questions;
	} 

	@Override
	public List<SecurityQuestion> listOfActiveSecurityQuestions() {
		Query qry=getSession().createQuery("FROM SecurityQuestion WHERE isActive = 1");
		List<SecurityQuestion> questions=qry.list();
		return questions;
	}

}
