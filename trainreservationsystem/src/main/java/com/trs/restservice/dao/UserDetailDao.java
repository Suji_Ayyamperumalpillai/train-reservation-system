package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.UserDetail;

public interface UserDetailDao {

	public void saveUserDetails(UserDetail userDetail);

	public void updateUserDetail(UserDetail userDetail);

	public void deleteUserDetail(int udId);

	public UserDetail getUserDetail(int udId);

	public List<UserDetail> getList();

	public boolean isAuthUser(UserDetail user);

	public UserDetail getUserByUserId(String userId);

	public boolean isUserExist(UserDetail user);
	

}
