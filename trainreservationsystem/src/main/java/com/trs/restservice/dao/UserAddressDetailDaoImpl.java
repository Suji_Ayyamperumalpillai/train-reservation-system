package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.UserAddressDetail;

@Repository
public class UserAddressDetailDaoImpl extends AbstractDao implements UserAddressDetailDao {

	@Override
	public void saveUserAddress(UserAddressDetail addressDetail) {
		getSession().save(addressDetail);
	}

	@Override
	public void updateUserAddress(UserAddressDetail addressDetail) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteUserAddress(int uadId) {
		Query delQry=getSession()
				.createQuery("DELETE FROM UserAddressDetail WHERE uadId = "+uadId);
		delQry.executeUpdate();
	}

	@Override
	public UserAddressDetail getUserAddress(int uadId) {
		UserAddressDetail addressDetail=getSession().get(UserAddressDetail.class, uadId);
		return addressDetail;
	}

	@Override
	public List<UserAddressDetail> list() {
		Query qry=getSession().createQuery("from UserAddressDetail");
		List<UserAddressDetail> list=qry.list();
		
		return list;
	}

}
