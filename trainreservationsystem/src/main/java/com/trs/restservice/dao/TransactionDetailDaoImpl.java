package com.trs.restservice.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.TransactionDetail;
import com.trs.restservice.models.UserDetail;

@Repository
public class TransactionDetailDaoImpl extends AbstractDao implements TransactionDetailDao{

	@Override
	public void saveTransactionDetail(TransactionDetail transactionDetail) {
		getSession().save(transactionDetail);
	}


	@Override
	public void deleteTransactionDetail(int tnsId) {
		Query delQry=getSession().createQuery("DELETE FROM TransactionDetail where tnsId= :tnsId");
		delQry.executeUpdate();

	}

	@Override
	public TransactionDetail getTransactionDetail(int tnsId) {
		TransactionDetail transactionDetail=getSession().get(TransactionDetail.class, tnsId);
		return transactionDetail;
	}

	@Override
	public List<TransactionDetail> getTransactionDetailByUserId(int udId) {
		/*Criteria criteria = getSession().createCriteria(TransactionDetail.class,"t")
				.createAlias("t.userDetail","u")
				.add(Restrictions.eq("u.udId", udId));*/
		
		Query query=getSession().createQuery("FROM TransactionDetail t WHERE t.userDetail.udId = :udId");
		query.setParameter("udId", udId);
		List<TransactionDetail> transactionDetail = query.list();

		return transactionDetail;
	}

	@Override
	public void changeReservationStatus(TransactionDetail transactionDetail) {
		getSession().update(transactionDetail);
		
	}
	
	//added by suji
	@Override
	public List<TransactionDetail> getBookedTicketsDetailByUserId(int udId) {
		Date todayDate=new Date();
		Criteria criteria = getSession().createCriteria(TransactionDetail.class,"t")
				.createAlias("t.userDetail","u")
				.add(Restrictions.eq("u.udId", udId))
				.add(Restrictions.eq("t.reservationStatus", "BOOKED"))
				.add(Restrictions.ge("t.dateOfJourney", todayDate));
		
		List<TransactionDetail> transactionDetail = criteria.list();

		return transactionDetail;
	}
	
	//added by shubham
	@Override
	 public TransactionDetail getLastTransactionDetailByUserId(int udId) {
	  
	  Criteria criteria = getSession().createCriteria(TransactionDetail.class,"ts")
	    .createAlias("ts.userDetail","u")
	    .addOrder(org.hibernate.criterion.Order.desc("tnsId"))
	    .setMaxResults(1)
	                .add(Restrictions.eq("u.udId", udId));

	  TransactionDetail lastTransactionDetailByUserId = (TransactionDetail) criteria.uniqueResult();

	  return lastTransactionDetailByUserId;
	 }
}
