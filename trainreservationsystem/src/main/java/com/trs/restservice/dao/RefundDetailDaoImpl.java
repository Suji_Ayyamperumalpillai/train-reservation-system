package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.CancelledTicketDetail;
import com.trs.restservice.models.RefundDetail;

@Repository
public class RefundDetailDaoImpl extends AbstractDao implements RefundDetailDao{

	@Override
	public void saveRefundDetail(RefundDetail refundDetail) {
	getSession().save(refundDetail);
		
	}

	@Override
	public List<RefundDetail> getRefundDetailList(int udId) {
		Query qry=getSession().createQuery("from RefundDetail rd WHERE "
				+ "rd.cancelledTicketDetail.transactionDetail.userDetail.udId= :udId");
		qry.setParameter("udId", udId);
		List<RefundDetail> refundDetail=qry.list();	
		return refundDetail;
	}

}
