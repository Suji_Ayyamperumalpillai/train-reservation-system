package com.trs.restservice.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.trs.restservice.models.AlertAndUpdateDetail;

@Repository
public class AlertAndUpdateDetailDaoImpl extends AbstractDao implements AlertAndUpdateDetailDao {

	@Override
	public void saveAlertsDetails(AlertAndUpdateDetail alertAndUpdateDetail) {
		getSession().save(alertAndUpdateDetail);
	}

	@Override
	public AlertAndUpdateDetail getAlertsDetailDao(int audId) {

		AlertAndUpdateDetail alertAndUpdateDetail=getSession().get(AlertAndUpdateDetail.class, audId);
		return alertAndUpdateDetail;
	}

	@Override
	public List<AlertAndUpdateDetail> list() {
		
		List<AlertAndUpdateDetail> list;
		Query qry=getSession().createQuery("from AlertAndUpdateDetail");
		
		list=qry.list();
		
		return list;
	}
	
	@Override
	public List<AlertAndUpdateDetail> listOfActiveUpdates() {
		
		List<AlertAndUpdateDetail> list;
		//if in table isActive = 1 then they are displayed on page
		Query qry=getSession().createQuery("from AlertAndUpdateDetail WHERE isActive = 1");
		
		list=qry.list();
		
		return list;
	}

	

}
