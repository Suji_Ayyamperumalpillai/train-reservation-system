package com.trs.restservice.dao;

import java.util.List;

import com.trs.restservice.models.City;

public interface CityDao {

	public City getCity(int cityId);
	
	public List<City> list();
	
	public List<City> listByStateId(int stateId);
}
