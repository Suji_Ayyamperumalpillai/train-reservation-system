package com.trs.restservice.date;

import java.util.Calendar;
import java.util.Date;

public class LastTimeOfDate {
	
	public Date getEndOfDay(Date day) {
	      return getEndOfDay(day,Calendar.getInstance());
	  }
	  
	  public  Date getEndOfDay(Date day,Calendar cal) {
	      if (day == null) day = new Date();
	      cal.setTime(day);
	      cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
	      cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
	      cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
	      cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
	      return cal.getTime();
	  }

}
