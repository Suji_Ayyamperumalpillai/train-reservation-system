package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the train_running_days database table.
 * 
 */
@Entity
@Table(name="train_running_days")
@NamedQuery(name="TrainRunningDay.findAll", query="SELECT t FROM TrainRunningDay t")
public class TrainRunningDay implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tr_id")
	private int trId;

	@Column(name="train_running_days")
	private String trainRunningDays;

	//bi-directional many-to-one association to TrainDetail
	@ManyToOne
	@JoinColumn(name="train_id")
	private TrainDetail trainDetail;

	public TrainRunningDay() {
	}

	public int getTrId() {
		return this.trId;
	}

	public void setTrId(int trId) {
		this.trId = trId;
	}

	public String getTrainRunningDays() {
		return this.trainRunningDays;
	}

	public void setTrainRunningDays(String trainRunningDays) {
		this.trainRunningDays = trainRunningDays;
	}

	public TrainDetail getTrainDetail() {
		return this.trainDetail;
	}

	public void setTrainDetail(TrainDetail trainDetail) {
		this.trainDetail = trainDetail;
	}

}