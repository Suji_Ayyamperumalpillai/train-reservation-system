package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the passenger_details database table.
 * 
 */
@Entity
@Table(name="passenger_details")
@NamedQuery(name="PassengerDetail.findAll", query="SELECT p FROM PassengerDetail p")
public class PassengerDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="psgd_id")
	private int psgdId;

	private int age;

	@Column(name="berth_preference")
	private String berthPreference;

	private String gender;

	@Column(name="passenger_name")
	private String passengerName;

	//bi-directional many-to-one association to TransactionDetail
	
	@ManyToOne
	@JoinColumn(name="tns_id")
	private TransactionDetail transactionDetail;

	public PassengerDetail() {
	}

	public int getPsgdId() {
		return this.psgdId;
	}

	public void setPsgdId(int psgdId) {
		this.psgdId = psgdId;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBerthPreference() {
		return this.berthPreference;
	}

	public void setBerthPreference(String berthPreference) {
		this.berthPreference = berthPreference;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassengerName() {
		return this.passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public TransactionDetail getTransactionDetail() {
		return this.transactionDetail;
	}

	public void setTransactionDetail(TransactionDetail transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

}