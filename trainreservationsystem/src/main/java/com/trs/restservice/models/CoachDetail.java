package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the coach_details database table.
 * 
 */
@Entity
@Table(name="coach_details")
@NamedQuery(name="CoachDetail.findAll", query="SELECT c FROM CoachDetail c")
public class CoachDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="coach_id")
	private int coachId;

	@Column(name="coach_name")
	private String coachName;

	@Column(name="total_seats_in_coach")
	private int totalSeatsInCoach;

	//bi-directional many-to-one association to SubCoachDetail
	@JsonIgnore
	@OneToMany(mappedBy="coachDetail")
	private List<SubCoachDetail> subCoachDetails;

	//bi-directional many-to-one association to TrainFareDetail
	@JsonIgnore
	@OneToMany(mappedBy="coachDetail")
	private List<TrainFareDetail> trainFareDetails;

	public CoachDetail() {
	}

	public int getCoachId() {
		return this.coachId;
	}

	public void setCoachId(int coachId) {
		this.coachId = coachId;
	}

	public String getCoachName() {
		return this.coachName;
	}

	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}

	public int getTotalSeatsInCoach() {
		return this.totalSeatsInCoach;
	}

	public void setTotalSeatsInCoach(int totalSeatsInCoach) {
		this.totalSeatsInCoach = totalSeatsInCoach;
	}

	public List<SubCoachDetail> getSubCoachDetails() {
		return this.subCoachDetails;
	}

	public void setSubCoachDetails(List<SubCoachDetail> subCoachDetails) {
		this.subCoachDetails = subCoachDetails;
	}

	public SubCoachDetail addSubCoachDetail(SubCoachDetail subCoachDetail) {
		getSubCoachDetails().add(subCoachDetail);
		subCoachDetail.setCoachDetail(this);

		return subCoachDetail;
	}

	public SubCoachDetail removeSubCoachDetail(SubCoachDetail subCoachDetail) {
		getSubCoachDetails().remove(subCoachDetail);
		subCoachDetail.setCoachDetail(null);

		return subCoachDetail;
	}

	public List<TrainFareDetail> getTrainFareDetails() {
		return this.trainFareDetails;
	}

	public void setTrainFareDetails(List<TrainFareDetail> trainFareDetails) {
		this.trainFareDetails = trainFareDetails;
	}

	public TrainFareDetail addTrainFareDetail(TrainFareDetail trainFareDetail) {
		getTrainFareDetails().add(trainFareDetail);
		trainFareDetail.setCoachDetail(this);

		return trainFareDetail;
	}

	public TrainFareDetail removeTrainFareDetail(TrainFareDetail trainFareDetail) {
		getTrainFareDetails().remove(trainFareDetail);
		trainFareDetail.setCoachDetail(null);

		return trainFareDetail;
	}

}