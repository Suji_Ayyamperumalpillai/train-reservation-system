package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;


/**
 * The persistent class for the train_schedule_details database table.
 * 
 */
@Entity
@Table(name="train_schedule_details")
@NamedQuery(name="TrainScheduleDetail.findAll", query="SELECT t FROM TrainScheduleDetail t")
public class TrainScheduleDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ts_id")
	private int tsId;

	@Column(name="arrival_time")
	private Time arrivalTime;

	private int day;

	@Column(name="departure_time")
	private Time departureTime;

	@Column(name="distance_in_km")
	private int distanceInKm;

	@Column(name="halt_time")
	private Time haltTime;

	@Column(name="station_code")
	private String stationCode;

	@Column(name="station_name")
	private String stationName;

	//bi-directional many-to-one association to TrainDetail
	@ManyToOne
	@JoinColumn(name="train_id")
	private TrainDetail trainDetail;

	public TrainScheduleDetail() {
	}

	public int getTsId() {
		return this.tsId;
	}

	public void setTsId(int tsId) {
		this.tsId = tsId;
	}

	public Time getArrivalTime() {
		return this.arrivalTime;
	}

	public void setArrivalTime(Time arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getDay() {
		return this.day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public Time getDepartureTime() {
		return this.departureTime;
	}

	public void setDepartureTime(Time departureTime) {
		this.departureTime = departureTime;
	}

	public int getDistanceInKm() {
		return this.distanceInKm;
	}

	public void setDistanceInKm(int distanceInKm) {
		this.distanceInKm = distanceInKm;
	}

	public Time getHaltTime() {
		return this.haltTime;
	}

	public void setHaltTime(Time haltTime) {
		this.haltTime = haltTime;
	}

	public String getStationCode() {
		return this.stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getStationName() {
		return this.stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public TrainDetail getTrainDetail() {
		return this.trainDetail;
	}

	public void setTrainDetail(TrainDetail trainDetail) {
		this.trainDetail = trainDetail;
	}

}