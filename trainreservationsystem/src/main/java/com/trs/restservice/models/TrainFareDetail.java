package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the train_fare_details database table.
 * 
 */
@Entity
@Table(name="train_fare_details")
@NamedQuery(name="TrainFareDetail.findAll", query="SELECT t FROM TrainFareDetail t")
public class TrainFareDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tf_id")
	private int tfId;

	@Column(name="fare_per_km")
	private int farePerKm;

	//bi-directional many-to-one association to CoachDetail
	@ManyToOne
	@JoinColumn(name="coach_id")
	private CoachDetail coachDetail;

	//bi-directional many-to-one association to TrainDetail
	@ManyToOne
	@JoinColumn(name="train_id")
	private TrainDetail trainDetail;

	public TrainFareDetail() {
	}

	public int getTfId() {
		return this.tfId;
	}

	public void setTfId(int tfId) {
		this.tfId = tfId;
	}

	public int getFarePerKm() {
		return this.farePerKm;
	}

	public void setFarePerKm(int farePerKm) {
		this.farePerKm = farePerKm;
	}

	public CoachDetail getCoachDetail() {
		return this.coachDetail;
	}

	public void setCoachDetail(CoachDetail coachDetail) {
		this.coachDetail = coachDetail;
	}

	public TrainDetail getTrainDetail() {
		return this.trainDetail;
	}

	public void setTrainDetail(TrainDetail trainDetail) {
		this.trainDetail = trainDetail;
	}

}