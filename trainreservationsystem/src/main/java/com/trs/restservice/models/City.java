package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the city database table.
 * 
 */
@Entity
@NamedQuery(name="City.findAll", query="SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="city_id")
	private int cityId;

	@Column(name="city_name")
	private String cityName;

	//bi-directional many-to-one association to State
	@ManyToOne
	@JoinColumn(name="state_id")
	private State state;

	//bi-directional many-to-one association to UserAddressDetail
	@OneToMany(mappedBy="city")
	@JsonIgnore
	private List<UserAddressDetail> userAddressDetails;

	public City() {
	}

	public int getCityId() {
		return this.cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<UserAddressDetail> getUserAddressDetails() {
		return this.userAddressDetails;
	}

	public void setUserAddressDetails(List<UserAddressDetail> userAddressDetails) {
		this.userAddressDetails = userAddressDetails;
	}

	public UserAddressDetail addUserAddressDetail(UserAddressDetail userAddressDetail) {
		getUserAddressDetails().add(userAddressDetail);
		userAddressDetail.setCity(this);

		return userAddressDetail;
	}

	public UserAddressDetail removeUserAddressDetail(UserAddressDetail userAddressDetail) {
		getUserAddressDetails().remove(userAddressDetail);
		userAddressDetail.setCity(null);

		return userAddressDetail;
	}

}