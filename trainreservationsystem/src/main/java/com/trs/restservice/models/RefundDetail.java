package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the refund_details database table.
 * 
 */
@Entity
@Table(name="refund_details")
@NamedQuery(name="RefundDetail.findAll", query="SELECT r FROM RefundDetail r")
public class RefundDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="r_id")
	private int rId;

	private String bank;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="refund_date")
	private Date refundDate;

	@Column(name="refund_status")
	private String refundStatus;

	//bi-directional many-to-one association to CancelledTicketDetail
	@ManyToOne
	@JoinColumn(name="ct_id")
	private CancelledTicketDetail cancelledTicketDetail;

	//bi-directional many-to-one association to TransactionDetail
	@ManyToOne
	@JoinColumn(name="tns_id")
	private TransactionDetail transactionDetail;

	public RefundDetail() {
	}

	public int getRId() {
		return this.rId;
	}

	public void setRId(int rId) {
		this.rId = rId;
	}

	public String getBank() {
		return this.bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public Date getRefundDate() {
		return this.refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}

	public String getRefundStatus() {
		return this.refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public CancelledTicketDetail getCancelledTicketDetail() {
		return this.cancelledTicketDetail;
	}

	public void setCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail) {
		this.cancelledTicketDetail = cancelledTicketDetail;
	}

	public TransactionDetail getTransactionDetail() {
		return this.transactionDetail;
	}

	public void setTransactionDetail(TransactionDetail transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

}