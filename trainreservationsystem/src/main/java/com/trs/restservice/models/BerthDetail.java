package com.trs.restservice.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the berth_details database table.
 * 
 */
@Entity
@Table(name="berth_details")
@NamedQuery(name="BerthDetail.findAll", query="SELECT b FROM BerthDetail b")
public class BerthDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="berth_id")
	private int berthId;

	@Column(name="berth_name")
	private String berthName;

	@Column(name="seat_number")
	private int seatNumber;

	private String status;

	//bi-directional many-to-one association to SeatBookingDetail
	@JsonIgnore
	@OneToMany(mappedBy="berthDetail")
	private List<SeatBookingDetail> seatBookingDetails;

	public BerthDetail() {
	}

	public int getBerthId() {
		return this.berthId;
	}

	public void setBerthId(int berthId) {
		this.berthId = berthId;
	}

	public String getBerthName() {
		return this.berthName;
	}

	public void setBerthName(String berthName) {
		this.berthName = berthName;
	}

	public int getSeatNumber() {
		return this.seatNumber;
	}

	public void setSeatNumber(int seatNumber) {
		this.seatNumber = seatNumber;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SeatBookingDetail> getSeatBookingDetails() {
		return this.seatBookingDetails;
	}

	public void setSeatBookingDetails(List<SeatBookingDetail> seatBookingDetails) {
		this.seatBookingDetails = seatBookingDetails;
	}

	public SeatBookingDetail addSeatBookingDetail(SeatBookingDetail seatBookingDetail) {
		getSeatBookingDetails().add(seatBookingDetail);
		seatBookingDetail.setBerthDetail(this);

		return seatBookingDetail;
	}

	public SeatBookingDetail removeSeatBookingDetail(SeatBookingDetail seatBookingDetail) {
		getSeatBookingDetails().remove(seatBookingDetail);
		seatBookingDetail.setBerthDetail(null);

		return seatBookingDetail;
	}

}