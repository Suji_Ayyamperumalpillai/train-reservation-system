package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the transaction_details database table.
 * 
 */
@Entity
@Table(name="transaction_details")
@NamedQuery(name="TransactionDetail.findAll", query="SELECT t FROM TransactionDetail t")
public class TransactionDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tns_id")
	private int tnsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_of_booking")
	private Date dateOfBooking;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_of_journey")
	private Date dateOfJourney;

	@Column(name="from_station")
	private String fromStation;

	@Column(name="pnr_number")
	private int pnrNumber;

	@Column(name="reservation_status")
	private String reservationStatus;

	@Column(name="seat_details")
	private String seatDetails;

	@Column(name="to_station")
	private String toStation;

	@Column(name="transaction_amount")
	private int transactionAmount;

	//bi-directional many-to-one association to CancelledTicketDetail
	@JsonIgnore
	@OneToMany(mappedBy="transactionDetail")
	private List<CancelledTicketDetail> cancelledTicketDetails;

	//bi-directional many-to-one association to PassengerDetail
	@JsonIgnore
	@OneToMany(mappedBy="transactionDetail")
	private List<PassengerDetail> passengerDetails;

	//bi-directional many-to-one association to RefundDetail
	@JsonIgnore
	@OneToMany(mappedBy="transactionDetail")
	private List<RefundDetail> refundDetails;

	//bi-directional many-to-one association to TrainDetail
	@ManyToOne
	@JoinColumn(name="train_id")
	private TrainDetail trainDetail;

	//bi-directional many-to-one association to UserDetail
	@ManyToOne
	@JoinColumn(name="ud_id")
	private UserDetail userDetail;

	public TransactionDetail() {
	}

	public int getTnsId() {
		return this.tnsId;
	}

	public void setTnsId(int tnsId) {
		this.tnsId = tnsId;
	}

	public Date getDateOfBooking() {
		return this.dateOfBooking;
	}

	public void setDateOfBooking(Date dateOfBooking) {
		this.dateOfBooking = dateOfBooking;
	}

	public Date getDateOfJourney() {
		return this.dateOfJourney;
	}

	public void setDateOfJourney(Date dateOfJourney) {
		this.dateOfJourney = dateOfJourney;
	}

	public String getFromStation() {
		return this.fromStation;
	}

	public void setFromStation(String fromStation) {
		this.fromStation = fromStation;
	}

	public int getPnrNumber() {
		return this.pnrNumber;
	}

	public void setPnrNumber(int pnrNumber) {
		this.pnrNumber = pnrNumber;
	}

	public String getReservationStatus() {
		return this.reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getSeatDetails() {
		return this.seatDetails;
	}

	public void setSeatDetails(String seatDetails) {
		this.seatDetails = seatDetails;
	}

	public String getToStation() {
		return this.toStation;
	}

	public void setToStation(String toStation) {
		this.toStation = toStation;
	}

	public int getTransactionAmount() {
		return this.transactionAmount;
	}

	public void setTransactionAmount(int transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public List<CancelledTicketDetail> getCancelledTicketDetails() {
		return this.cancelledTicketDetails;
	}

	public void setCancelledTicketDetails(List<CancelledTicketDetail> cancelledTicketDetails) {
		this.cancelledTicketDetails = cancelledTicketDetails;
	}

	public CancelledTicketDetail addCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail) {
		getCancelledTicketDetails().add(cancelledTicketDetail);
		cancelledTicketDetail.setTransactionDetail(this);

		return cancelledTicketDetail;
	}

	public CancelledTicketDetail removeCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail) {
		getCancelledTicketDetails().remove(cancelledTicketDetail);
		cancelledTicketDetail.setTransactionDetail(null);

		return cancelledTicketDetail;
	}

	public List<PassengerDetail> getPassengerDetails() {
		return this.passengerDetails;
	}

	public void setPassengerDetails(List<PassengerDetail> passengerDetails) {
		this.passengerDetails = passengerDetails;
	}

	public PassengerDetail addPassengerDetail(PassengerDetail passengerDetail) {
		getPassengerDetails().add(passengerDetail);
		passengerDetail.setTransactionDetail(this);

		return passengerDetail;
	}

	public PassengerDetail removePassengerDetail(PassengerDetail passengerDetail) {
		getPassengerDetails().remove(passengerDetail);
		passengerDetail.setTransactionDetail(null);

		return passengerDetail;
	}

	public List<RefundDetail> getRefundDetails() {
		return this.refundDetails;
	}

	public void setRefundDetails(List<RefundDetail> refundDetails) {
		this.refundDetails = refundDetails;
	}

	public RefundDetail addRefundDetail(RefundDetail refundDetail) {
		getRefundDetails().add(refundDetail);
		refundDetail.setTransactionDetail(this);

		return refundDetail;
	}

	public RefundDetail removeRefundDetail(RefundDetail refundDetail) {
		getRefundDetails().remove(refundDetail);
		refundDetail.setTransactionDetail(null);

		return refundDetail;
	}

	public TrainDetail getTrainDetail() {
		return this.trainDetail;
	}

	public void setTrainDetail(TrainDetail trainDetail) {
		this.trainDetail = trainDetail;
	}

	public UserDetail getUserDetail() {
		return this.userDetail;
	}

	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}

}