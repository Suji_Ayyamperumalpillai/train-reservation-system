package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the alert_and_update_details database table.
 * 
 */
@Entity
@Table(name="alert_and_update_details")
@NamedQuery(name="AlertAndUpdateDetail.findAll", query="SELECT a FROM AlertAndUpdateDetail a")
public class AlertAndUpdateDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="au_id")
	private int auId;

	@Column(name="alert_and_update_info")
	private String alertAndUpdateInfo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="is_active")
	private int isActive;

	public AlertAndUpdateDetail() {
	}

	public int getAuId() {
		return this.auId;
	}

	public void setAuId(int auId) {
		this.auId = auId;
	}

	public String getAlertAndUpdateInfo() {
		return this.alertAndUpdateInfo;
	}

	public void setAlertAndUpdateInfo(String alertAndUpdateInfo) {
		this.alertAndUpdateInfo = alertAndUpdateInfo;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getIsActive() {
		return this.isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

}