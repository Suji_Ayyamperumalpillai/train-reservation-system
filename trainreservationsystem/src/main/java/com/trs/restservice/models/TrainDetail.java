package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the train_details database table.
 * 
 */
@Entity
@Table(name="train_details")
@NamedQuery(name="TrainDetail.findAll", query="SELECT t FROM TrainDetail t")
public class TrainDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="train_id")
	private int trainId;

	@Column(name="destination_station")
	private String destinationStation;

	@Column(name="source_station")
	private String sourceStation;

	@Column(name="train_name")
	private String trainName;

	@Column(name="train_number")
	private int trainNumber;

	//bi-directional many-to-one association to SeatBookingDetail
	@JsonIgnore
	@OneToMany(mappedBy="trainDetail")
	private List<SeatBookingDetail> seatBookingDetails;

	//bi-directional many-to-one association to TrainFareDetail
	@JsonIgnore
	@OneToMany(mappedBy="trainDetail")
	private List<TrainFareDetail> trainFareDetails;

	//bi-directional many-to-one association to TrainRunningDay
	@JsonIgnore
	@OneToMany(mappedBy="trainDetail")
	private List<TrainRunningDay> trainRunningDays;

	//bi-directional many-to-one association to TrainScheduleDetail
	@JsonIgnore
	@OneToMany(mappedBy="trainDetail")
	private List<TrainScheduleDetail> trainScheduleDetails;

	//bi-directional many-to-one association to TransactionDetail
	@OneToMany(mappedBy="trainDetail")
	@JsonIgnore
	private List<TransactionDetail> transactionDetails;

	public TrainDetail() {
	}

	public int getTrainId() {
		return this.trainId;
	}

	public void setTrainId(int trainId) {
		this.trainId = trainId;
	}

	public String getDestinationStation() {
		return this.destinationStation;
	}

	public void setDestinationStation(String destinationStation) {
		this.destinationStation = destinationStation;
	}

	public String getSourceStation() {
		return this.sourceStation;
	}

	public void setSourceStation(String sourceStation) {
		this.sourceStation = sourceStation;
	}

	public String getTrainName() {
		return this.trainName;
	}

	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}

	public int getTrainNumber() {
		return this.trainNumber;
	}

	public void setTrainNumber(int trainNumber) {
		this.trainNumber = trainNumber;
	}

	public List<SeatBookingDetail> getSeatBookingDetails() {
		return this.seatBookingDetails;
	}

	public void setSeatBookingDetails(List<SeatBookingDetail> seatBookingDetails) {
		this.seatBookingDetails = seatBookingDetails;
	}

	public SeatBookingDetail addSeatBookingDetail(SeatBookingDetail seatBookingDetail) {
		getSeatBookingDetails().add(seatBookingDetail);
		seatBookingDetail.setTrainDetail(this);

		return seatBookingDetail;
	}

	public SeatBookingDetail removeSeatBookingDetail(SeatBookingDetail seatBookingDetail) {
		getSeatBookingDetails().remove(seatBookingDetail);
		seatBookingDetail.setTrainDetail(null);

		return seatBookingDetail;
	}

	public List<TrainFareDetail> getTrainFareDetails() {
		return this.trainFareDetails;
	}

	public void setTrainFareDetails(List<TrainFareDetail> trainFareDetails) {
		this.trainFareDetails = trainFareDetails;
	}

	public TrainFareDetail addTrainFareDetail(TrainFareDetail trainFareDetail) {
		getTrainFareDetails().add(trainFareDetail);
		trainFareDetail.setTrainDetail(this);

		return trainFareDetail;
	}

	public TrainFareDetail removeTrainFareDetail(TrainFareDetail trainFareDetail) {
		getTrainFareDetails().remove(trainFareDetail);
		trainFareDetail.setTrainDetail(null);

		return trainFareDetail;
	}

	public List<TrainRunningDay> getTrainRunningDays() {
		return this.trainRunningDays;
	}

	public void setTrainRunningDays(List<TrainRunningDay> trainRunningDays) {
		this.trainRunningDays = trainRunningDays;
	}

	public TrainRunningDay addTrainRunningDay(TrainRunningDay trainRunningDay) {
		getTrainRunningDays().add(trainRunningDay);
		trainRunningDay.setTrainDetail(this);

		return trainRunningDay;
	}

	public TrainRunningDay removeTrainRunningDay(TrainRunningDay trainRunningDay) {
		getTrainRunningDays().remove(trainRunningDay);
		trainRunningDay.setTrainDetail(null);

		return trainRunningDay;
	}

	public List<TrainScheduleDetail> getTrainScheduleDetails() {
		return this.trainScheduleDetails;
	}

	public void setTrainScheduleDetails(List<TrainScheduleDetail> trainScheduleDetails) {
		this.trainScheduleDetails = trainScheduleDetails;
	}

	public TrainScheduleDetail addTrainScheduleDetail(TrainScheduleDetail trainScheduleDetail) {
		getTrainScheduleDetails().add(trainScheduleDetail);
		trainScheduleDetail.setTrainDetail(this);

		return trainScheduleDetail;
	}

	public TrainScheduleDetail removeTrainScheduleDetail(TrainScheduleDetail trainScheduleDetail) {
		getTrainScheduleDetails().remove(trainScheduleDetail);
		trainScheduleDetail.setTrainDetail(null);

		return trainScheduleDetail;
	}

	public List<TransactionDetail> getTransactionDetails() {
		return this.transactionDetails;
	}

	public void setTransactionDetails(List<TransactionDetail> transactionDetails) {
		this.transactionDetails = transactionDetails;
	}

	public TransactionDetail addTransactionDetail(TransactionDetail transactionDetail) {
		getTransactionDetails().add(transactionDetail);
		transactionDetail.setTrainDetail(this);

		return transactionDetail;
	}

	public TransactionDetail removeTransactionDetail(TransactionDetail transactionDetail) {
		getTransactionDetails().remove(transactionDetail);
		transactionDetail.setTrainDetail(null);

		return transactionDetail;
	}

}