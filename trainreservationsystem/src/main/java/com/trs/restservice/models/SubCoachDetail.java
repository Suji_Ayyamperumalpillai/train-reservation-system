package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the sub_coach_details database table.
 * 
 */
@Entity
@Table(name="sub_coach_details")
@NamedQuery(name="SubCoachDetail.findAll", query="SELECT s FROM SubCoachDetail s")
public class SubCoachDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sc_id")
	private int scId;

	@Column(name="sub_coach_name")
	private String subCoachName;

	@Column(name="total_seats")
	private int totalSeats;

	//bi-directional many-to-one association to SeatBookingDetail
	@JsonIgnore
	@OneToMany(mappedBy="subCoachDetail")
	private List<SeatBookingDetail> seatBookingDetails;

	//bi-directional many-to-one association to CoachDetail
	@ManyToOne
	@JoinColumn(name="coach_id")
	private CoachDetail coachDetail;

	public SubCoachDetail() {
	}

	public int getScId() {
		return this.scId;
	}

	public void setScId(int scId) {
		this.scId = scId;
	}

	public String getSubCoachName() {
		return this.subCoachName;
	}

	public void setSubCoachName(String subCoachName) {
		this.subCoachName = subCoachName;
	}

	public int getTotalSeats() {
		return this.totalSeats;
	}

	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}

	public List<SeatBookingDetail> getSeatBookingDetails() {
		return this.seatBookingDetails;
	}

	public void setSeatBookingDetails(List<SeatBookingDetail> seatBookingDetails) {
		this.seatBookingDetails = seatBookingDetails;
	}

	public SeatBookingDetail addSeatBookingDetail(SeatBookingDetail seatBookingDetail) {
		getSeatBookingDetails().add(seatBookingDetail);
		seatBookingDetail.setSubCoachDetail(this);

		return seatBookingDetail;
	}

	public SeatBookingDetail removeSeatBookingDetail(SeatBookingDetail seatBookingDetail) {
		getSeatBookingDetails().remove(seatBookingDetail);
		seatBookingDetail.setSubCoachDetail(null);

		return seatBookingDetail;
	}

	public CoachDetail getCoachDetail() {
		return this.coachDetail;
	}

	public void setCoachDetail(CoachDetail coachDetail) {
		this.coachDetail = coachDetail;
	}

}