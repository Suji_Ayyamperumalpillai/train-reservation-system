package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the cancelled_ticket_details database table.
 * 
 */
@Entity
@Table(name="cancelled_ticket_details")
@NamedQuery(name="CancelledTicketDetail.findAll", query="SELECT c FROM CancelledTicketDetail c")
public class CancelledTicketDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ct_id")
	private int ctId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_of_cancellation")
	private Date dateOfCancellation;

	@Column(name="refund_amount")
	private int refundAmount;

	//bi-directional many-to-one association to TransactionDetail
	@ManyToOne
	@JoinColumn(name="tns_id")
	private TransactionDetail transactionDetail;

	//bi-directional many-to-one association to RefundDetail
	@JsonIgnore
	@OneToMany(mappedBy="cancelledTicketDetail")
	private List<RefundDetail> refundDetails;

	public CancelledTicketDetail() {
	}

	public int getCtId() {
		return this.ctId;
	}

	public void setCtId(int ctId) {
		this.ctId = ctId;
	}

	public Date getDateOfCancellation() {
		return this.dateOfCancellation;
	}

	public void setDateOfCancellation(Date dateOfCancellation) {
		this.dateOfCancellation = dateOfCancellation;
	}

	public int getRefundAmount() {
		return this.refundAmount;
	}

	public void setRefundAmount(int refundAmount) {
		this.refundAmount = refundAmount;
	}

	public TransactionDetail getTransactionDetail() {
		return this.transactionDetail;
	}

	public void setTransactionDetail(TransactionDetail transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public List<RefundDetail> getRefundDetails() {
		return this.refundDetails;
	}

	public void setRefundDetails(List<RefundDetail> refundDetails) {
		this.refundDetails = refundDetails;
	}

	public RefundDetail addRefundDetail(RefundDetail refundDetail) {
		getRefundDetails().add(refundDetail);
		refundDetail.setCancelledTicketDetail(this);

		return refundDetail;
	}

	public RefundDetail removeRefundDetail(RefundDetail refundDetail) {
		getRefundDetails().remove(refundDetail);
		refundDetail.setCancelledTicketDetail(null);

		return refundDetail;
	}

}