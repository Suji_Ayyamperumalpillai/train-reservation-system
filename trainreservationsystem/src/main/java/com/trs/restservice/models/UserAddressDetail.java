package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_address_details database table.
 * 
 */
@Entity
@Table(name="user_address_details")
@NamedQuery(name="UserAddressDetail.findAll", query="SELECT u FROM UserAddressDetail u")
public class UserAddressDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="uad_id")
	private int uadId;

	private String area;

	private int pincode;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="city_id")
	private City city;

	//bi-directional many-to-one association to UserDetail
	@ManyToOne
	@JoinColumn(name="ud_id")
	private UserDetail userDetail;

	public UserAddressDetail() {
	}

	public int getUadId() {
		return this.uadId;
	}

	public void setUadId(int uadId) {
		this.uadId = uadId;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public int getPincode() {
		return this.pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public UserDetail getUserDetail() {
		return this.userDetail;
	}

	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}

}