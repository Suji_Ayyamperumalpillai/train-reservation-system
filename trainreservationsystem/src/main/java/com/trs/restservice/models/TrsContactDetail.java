package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the trs_contact_details database table.
 * 
 */
@Entity
@Table(name="trs_contact_details")
@NamedQuery(name="TrsContactDetail.findAll", query="SELECT t FROM TrsContactDetail t")
public class TrsContactDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="trs_contact_id")
	private int trsContactId;

	@Column(name="contact_email")
	private String contactEmail;

	@Column(name="contact_info")
	private String contactInfo;

	@Column(name="contact_number")
	private String contactNumber;

	public TrsContactDetail() {
	}

	public int getTrsContactId() {
		return this.trsContactId;
	}

	public void setTrsContactId(int trsContactId) {
		this.trsContactId = trsContactId;
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactInfo() {
		return this.contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getContactNumber() {
		return this.contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}