package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the user_details database table.
 * 
 */
@Entity
@Table(name="user_details")
@NamedQuery(name="UserDetail.findAll", query="SELECT u FROM UserDetail u")
public class UserDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ud_id")
	private int udId;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_birth")
	private Date dateOfBirth;

	@Column(name="email_id")
	private String emailId;

	@Column(name="first_name")
	private String firstName;

	private String gender;

	@Column(name="last_name")
	private String lastName;

	@Column(name="mobile_number")
	private String mobileNumber;

	private String nationality;

	@Column(name="security_answer")
	private String securityAnswer;

	@Column(name="security_question")
	private String securityQuestion;

	@Column(name="user_id")
	private String userId;

	@Column(name="user_password")
	private String userPassword;

	//bi-directional many-to-one association to TransactionDetail
	@JsonIgnore
	@OneToMany(mappedBy="userDetail")
	private List<TransactionDetail> transactionDetails;

	//bi-directional many-to-one association to UserAddressDetail
	@JsonIgnore
	@OneToMany(mappedBy="userDetail")
	private List<UserAddressDetail> userAddressDetails;

	public UserDetail() {
	}

	public int getUdId() {
		return this.udId;
	}

	public void setUdId(int udId) {
		this.udId = udId;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getSecurityAnswer() {
		return this.securityAnswer;
	}

	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

	public String getSecurityQuestion() {
		return this.securityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public List<TransactionDetail> getTransactionDetails() {
		return this.transactionDetails;
	}

	public void setTransactionDetails(List<TransactionDetail> transactionDetails) {
		this.transactionDetails = transactionDetails;
	}

	public TransactionDetail addTransactionDetail(TransactionDetail transactionDetail) {
		getTransactionDetails().add(transactionDetail);
		transactionDetail.setUserDetail(this);

		return transactionDetail;
	}

	public TransactionDetail removeTransactionDetail(TransactionDetail transactionDetail) {
		getTransactionDetails().remove(transactionDetail);
		transactionDetail.setUserDetail(null);

		return transactionDetail;
	}

	public List<UserAddressDetail> getUserAddressDetails() {
		return this.userAddressDetails;
	}

	public void setUserAddressDetails(List<UserAddressDetail> userAddressDetails) {
		this.userAddressDetails = userAddressDetails;
	}

	public UserAddressDetail addUserAddressDetail(UserAddressDetail userAddressDetail) {
		getUserAddressDetails().add(userAddressDetail);
		userAddressDetail.setUserDetail(this);

		return userAddressDetail;
	}

	public UserAddressDetail removeUserAddressDetail(UserAddressDetail userAddressDetail) {
		getUserAddressDetails().remove(userAddressDetail);
		userAddressDetail.setUserDetail(null);

		return userAddressDetail;
	}

}