package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the seat_booking_details database table.
 * 
 */
@Entity
@Table(name="seat_booking_details")
@NamedQuery(name="SeatBookingDetail.findAll", query="SELECT s FROM SeatBookingDetail s")
public class SeatBookingDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sb_id")
	private int sbId;

	@Temporal(TemporalType.DATE)
	@Column(name="booking_for_date")
	private Date bookingForDate;

	@Column(name="booking_status")
	private String bookingStatus;

	//bi-directional many-to-one association to BerthDetail
	@ManyToOne
	@JoinColumn(name="berth_id")
	private BerthDetail berthDetail;

	//bi-directional many-to-one association to SubCoachDetail
	@ManyToOne
	@JoinColumn(name="sc_id")
	private SubCoachDetail subCoachDetail;

	//bi-directional many-to-one association to TrainDetail
	@ManyToOne
	@JoinColumn(name="train_id")
	private TrainDetail trainDetail;

	public SeatBookingDetail() {
	}

	public int getSbId() {
		return this.sbId;
	}

	public void setSbId(int sbId) {
		this.sbId = sbId;
	}

	public Date getBookingForDate() {
		return this.bookingForDate;
	}

	public void setBookingForDate(Date bookingForDate) {
		this.bookingForDate = bookingForDate;
	}

	public String getBookingStatus() {
		return this.bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public BerthDetail getBerthDetail() {
		return this.berthDetail;
	}

	public void setBerthDetail(BerthDetail berthDetail) {
		this.berthDetail = berthDetail;
	}

	public SubCoachDetail getSubCoachDetail() {
		return this.subCoachDetail;
	}

	public void setSubCoachDetail(SubCoachDetail subCoachDetail) {
		this.subCoachDetail = subCoachDetail;
	}

	public TrainDetail getTrainDetail() {
		return this.trainDetail;
	}

	public void setTrainDetail(TrainDetail trainDetail) {
		this.trainDetail = trainDetail;
	}

}