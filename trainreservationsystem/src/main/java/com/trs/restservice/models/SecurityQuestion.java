package com.trs.restservice.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the security_questions database table.
 * 
 */
@Entity
@Table(name="security_questions")
@NamedQuery(name="SecurityQuestion.findAll", query="SELECT s FROM SecurityQuestion s")
public class SecurityQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sq_id")
	private int sqId;

	@Column(name="is_active")
	private int isActive;

	@Column(name="security_question")
	private String securityQuestion;

	public SecurityQuestion() {
	}

	public int getSqId() {
		return this.sqId;
	}

	public void setSqId(int sqId) {
		this.sqId = sqId;
	}

	public int getIsActive() {
		return this.isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getSecurityQuestion() {
		return this.securityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

}