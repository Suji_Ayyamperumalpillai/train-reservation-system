package com.trs.restservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.AlertAndUpdateDetailService;

@RestController
@CrossOrigin(origins="*")
public class AlertAndUpdateController {

	@Autowired
	private AlertAndUpdateDetailService alertAndUpdateDetailService;
	
	@RequestMapping(value="/getactivealerts",method=RequestMethod.GET)
	public ResponseEntity<?> getActiveAlertsList(){
		
		try {
			
			return new ResponseEntity<>(alertAndUpdateDetailService.listOfActiveUpdates(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	@RequestMapping(value="/getalerts",method=RequestMethod.GET)
	public ResponseEntity<?> getAlertsList(){
		
		try {
			
			return new ResponseEntity<>(alertAndUpdateDetailService.list(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
}
