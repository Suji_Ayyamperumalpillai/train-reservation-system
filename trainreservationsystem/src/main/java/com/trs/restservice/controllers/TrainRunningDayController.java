package com.trs.restservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.TrainRunningDayService;

@RestController
@CrossOrigin(origins="*")
public class TrainRunningDayController {
	
	@Autowired
	TrainRunningDayService  trainRunningDayService;
	
	@RequestMapping(value="/gettrainrunningDay/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> getTrainRunningDay(@PathVariable("id")int trainId){
		
		try {
			
			return new ResponseEntity<>(trainRunningDayService.list(trainId),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}

}
