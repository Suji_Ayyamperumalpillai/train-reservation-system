package com.trs.restservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.CityService;

@RestController
@CrossOrigin(origins="*")
public class CityResource {

	@Autowired
	private CityService cityService;
	
	@RequestMapping(value="/getcities",method=RequestMethod.GET)
	public ResponseEntity<?> getCities(){
		
		try {
			
			return new ResponseEntity<>(cityService.list(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	@RequestMapping(value="/getcities/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> getCitiesbyStateId(@PathVariable("id") int stateId){
		
		try {
			
			return new ResponseEntity<>(cityService.listByStateId(stateId),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
}