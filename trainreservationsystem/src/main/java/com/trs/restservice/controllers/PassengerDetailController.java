package com.trs.restservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.PassengerDetail;
import com.trs.restservice.models.UserDetail;
import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.PassengerDetailService;

@RestController
@CrossOrigin(origins="*")
public class PassengerDetailController {
	@Autowired
	private PassengerDetailService passengerDetailService;
	
	
	@RequestMapping(value="/getpessengerdetails",method=RequestMethod.GET)
	public ResponseEntity<?> getPessengerDetails(){
		try {

			return new ResponseEntity<>(passengerDetailService.getPessengerDetails(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	@RequestMapping(value="/addpassenger",method=RequestMethod.POST)
	public ResponseEntity<?> savePassengerDetails(@RequestBody PassengerDetail passengerDetail){
		try {

			passengerDetailService.savePassengerDetails(passengerDetail);
			return new ResponseEntity<>(passengerDetailService.getPassengerByTransactionId(passengerDetail.getTransactionDetail().getTnsId()),HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}
	
}
