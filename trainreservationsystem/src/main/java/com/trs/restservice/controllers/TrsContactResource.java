package com.trs.restservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.TrsContactDetailService;

@RestController
@CrossOrigin(origins="*")
public class TrsContactResource {
	@Autowired
	private TrsContactDetailService contactDetailService;
	
	@RequestMapping(value="/getcontacts",method=RequestMethod.GET)
	public ResponseEntity<?> getContactDetails(){
		
		try {
			
			return new ResponseEntity<>(contactDetailService.list(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}

}
