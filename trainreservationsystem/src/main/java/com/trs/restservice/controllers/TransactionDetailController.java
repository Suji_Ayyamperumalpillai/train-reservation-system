package com.trs.restservice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.TransactionDetail;
import com.trs.restservice.models.UserDetail;
import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.TransactionDetailService;


@RestController
@CrossOrigin(origins="*")
public class TransactionDetailController {
	@Autowired
	private TransactionDetailService transactionDetailService;

	@RequestMapping(value="/gettickethistoryuserid/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> gettickethistoryuserid(@PathVariable("id")int udId){
		List<TransactionDetail> transactionDetailList;
		try {
			transactionDetailList = transactionDetailService.getTransactionDetailByUserId(udId);
			return new ResponseEntity<>(transactionDetailList,HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	
	//added by suji
	@RequestMapping(value="/getbookedtickets/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> getBookedTicketsByUserId(@PathVariable("id")int udId){
		List<TransactionDetail> transactionDetailList;
		try {
			transactionDetailList = transactionDetailService.getBookedTicketsByUserId(udId);
			return new ResponseEntity<>(transactionDetailList,HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	//
	
	
	@RequestMapping(value="/addtransaction",method=RequestMethod.POST)
	public ResponseEntity<?> saveTransactionDetail(@RequestBody TransactionDetail transactionDetail){
		try {

			transactionDetailService.saveTransactionDetail(transactionDetail);
			return new ResponseEntity<>(HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}
	
	
	@RequestMapping(value="/changereservationstatus",method=RequestMethod.PUT)
	public ResponseEntity<?> changeReservationStatus(@RequestBody TransactionDetail transactionDetail){
		try {
			transactionDetailService.changeReservationStatus(transactionDetail);
			return new ResponseEntity<>(HttpStatus.OK);
			//return "success";
		} 
		catch (Exception e) {
			//return "failed";
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="/getlasttickethistoryuserid/{id}",method=RequestMethod.GET)
	 public ResponseEntity<?> getLastTicketHistoryByUserId(@PathVariable("id")int udId){
	  TransactionDetail transactionDetailList;
	  try {
	   transactionDetailList = transactionDetailService.getLastTransactionDetailByUserId(udId);
	   return new ResponseEntity<>(transactionDetailList,HttpStatus.OK);
	  } 
	  catch (Exception e) {
	   String errorMessage=e+" ---------- Error";
	   ErrorMessage error=new ErrorMessage();
	   error.setMessage(errorMessage);
	   error.setStatus(HttpStatus.BAD_REQUEST);
	   return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
	  }

	 }

}
