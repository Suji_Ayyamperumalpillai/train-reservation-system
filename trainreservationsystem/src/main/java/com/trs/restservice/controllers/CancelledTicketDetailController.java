package com.trs.restservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.CancelledTicketDetail;
import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.CancelledTicketDetailService;

@RestController
@CrossOrigin(origins="*")
public class CancelledTicketDetailController {
	
	@Autowired
	private CancelledTicketDetailService cancelledTicketDetailService;
	
	@RequestMapping(value="/cancelticket",method=RequestMethod.POST)
	public ResponseEntity<?> saveCancelledTicketDetail(@RequestBody CancelledTicketDetail cancelledTicketDetail){
		try {
			
			cancelledTicketDetailService.saveCancelledTicketDetail(cancelledTicketDetail);
			return new ResponseEntity<>(cancelledTicketDetailService.getCancelledTicketDetail(cancelledTicketDetail),HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}
}
