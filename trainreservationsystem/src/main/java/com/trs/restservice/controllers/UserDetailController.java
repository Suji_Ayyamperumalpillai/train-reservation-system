package com.trs.restservice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.UserDetail;
import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.UserDetailService;

@RestController
@CrossOrigin(origins="*")
public class UserDetailController {

	@Autowired
	private UserDetailService userDetailService;

	@RequestMapping(value="/getuser/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable("id")int udId){
		
		try {
			
			return new ResponseEntity<>(userDetailService.getUserDetail(udId),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value="/getusers",method=RequestMethod.GET)
	public ResponseEntity<?> getusersList(){
		List<UserDetail> list;
		try {
			list = userDetailService.getList();
			return new ResponseEntity<>(list,HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}


	@RequestMapping(value="/adduser",method=RequestMethod.POST)
	public ResponseEntity<?> saveUser(@RequestBody UserDetail userDetail){
		try {

			userDetailService.saveUserDetails(userDetail);
			return new ResponseEntity<>(userDetailService.getUserById(userDetail.getUserId()),HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}


	@RequestMapping(value="/updateuser",method=RequestMethod.PUT)
	public ResponseEntity<?> updateUser(@RequestBody UserDetail userDetail){
		try {
			userDetailService.updateUserDetail(userDetail);
			return new ResponseEntity<>(userDetailService.getList(),HttpStatus.OK);
			//return "success";
		} 
		catch (Exception e) {
			//return "failed";
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value="/deleteuser/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteUserbyId(@PathVariable("id")int udId){
		try {
			userDetailService.deleteUserDetail(udId);
			return new ResponseEntity<>(userDetailService.getList(),HttpStatus.OK);
			//	return "success";
		} 
		catch (Exception e) {
			//return "failed";
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}
	}
	@RequestMapping(value="/isuserexist",method=RequestMethod.POST)
	public ResponseEntity<?> isUserExist(@RequestBody UserDetail user){
		String errorMessage;
		ErrorMessage error;
		try{
			if(userDetailService.isUserExist(user)){
				errorMessage="This User ID already exists!";
				error=new ErrorMessage();
				error.setMessage(errorMessage);
				error.setStatus(HttpStatus.OK);
				return new ResponseEntity<ErrorMessage>(error,HttpStatus.OK);
			}
			else{
				errorMessage="This User ID does not exists!";
				error=new ErrorMessage();
				error.setMessage(errorMessage);
				error.setStatus(HttpStatus.BAD_REQUEST);
				return new ResponseEntity<>(error,HttpStatus.BAD_REQUEST);
			}
		}
		catch(Exception e){
			errorMessage=e+" ---------- Error";
			error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}
	}
	
	//added by shubham
	@RequestMapping(value="/authuser",method=RequestMethod.POST)
	public ResponseEntity<?> authorizeUser(@RequestBody UserDetail user){
		String errorMessage="";
		ErrorMessage error;
		try{
			if(userDetailService.isAuthUser(user)){
				UserDetail user1=new UserDetail();
				user1=userDetailService.getUserById(user.getUserId());
				return new ResponseEntity<>(user1,HttpStatus.OK);
			}
			else{
				errorMessage="unauthorized user";
				error=new ErrorMessage();
				error.setMessage(errorMessage);
				error.setStatus(HttpStatus.BAD_REQUEST);
				return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
			}
		}
		catch(Exception e){
			errorMessage=e+" ---------- Error";
			error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="/getuserbyuserid/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> getUserByUserId(@PathVariable("id")String userId){
		
		try {
			 UserDetail userDetail = userDetailService.getUserById(userId);
			if(userDetail!=null){
				System.out.println(userId);
				return new ResponseEntity<>(userDetail,HttpStatus.OK);
			}
			else{
				String errorMessage="user id dosen't exist";
				ErrorMessage error=new ErrorMessage();
				error.setMessage(errorMessage);
				error.setStatus(HttpStatus.BAD_REQUEST);
				return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
			}
			
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
}
