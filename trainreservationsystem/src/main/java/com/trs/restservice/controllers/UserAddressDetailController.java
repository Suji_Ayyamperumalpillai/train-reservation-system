package com.trs.restservice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.UserAddressDetail;
import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.UserAddressDetailService;

@RestController
@CrossOrigin(origins="*")
public class UserAddressDetailController {

	@Autowired
	private UserAddressDetailService userAddressDetailService;

	@RequestMapping(value="/getaddress/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> getUserAddress(@PathVariable("id")int uadId){
		
		try {
			
			return new ResponseEntity<>(userAddressDetailService.getUserAddress(uadId),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value="/getaddresses",method=RequestMethod.GET)
	public ResponseEntity<?> getUsersAddressList(){
		List<UserAddressDetail> list;
		try {
			list = userAddressDetailService.list();
			return new ResponseEntity<>(list,HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}


	@RequestMapping(value="/adduseraddress",method=RequestMethod.POST)
	public ResponseEntity<?> saveUserAddress(@RequestBody UserAddressDetail addressDetail){
		try {

			userAddressDetailService.saveUserAddress(addressDetail);
			return new ResponseEntity<>(userAddressDetailService.list(),HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}


	@RequestMapping(value="/updateaddress",method=RequestMethod.PUT)
	public ResponseEntity<?> updateUserAddress(@RequestBody UserAddressDetail addressDetail){
		try {
			userAddressDetailService.updateUserAddress(addressDetail);
			return new ResponseEntity<>(userAddressDetailService.list(),HttpStatus.OK);

		} 
		catch (Exception e) {
			//return "failed";
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value="/deleteaddress/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteUserAddressById(@PathVariable("id")int uadId){
		try {
			userAddressDetailService.deleteUserAddress(uadId);
			return new ResponseEntity<>(userAddressDetailService.list(),HttpStatus.OK);
			//	return "success";
		} 
		catch (Exception e) {
			//return "failed";
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}
	}
}
