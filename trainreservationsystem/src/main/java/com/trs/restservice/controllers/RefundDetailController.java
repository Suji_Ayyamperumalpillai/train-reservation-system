package com.trs.restservice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.RefundDetail;

import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.RefundDetailService;


@RestController
@CrossOrigin(origins="*")
public class RefundDetailController {

	@Autowired
	private RefundDetailService refundDetailService;

	@RequestMapping(value="/getrefunddetails/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> getusersList(@PathVariable("id")int udId){
		List<RefundDetail> list;
		try {
			list = refundDetailService.getRefundDetails(udId);
			return new ResponseEntity<>(list,HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}



	@RequestMapping(value="/addrefunddetails",method=RequestMethod.POST)
	public ResponseEntity<?> saveRefundDetails(@RequestBody RefundDetail refundDetail){
		try {
			refundDetailService.saveRefundDetails(refundDetail);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
		 
		
	}
}
