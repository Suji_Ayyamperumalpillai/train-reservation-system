package com.trs.restservice.controllers;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.SeatBookingDetail;
import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.SeatBookingDetailService;

@RestController
@CrossOrigin(origins="*")
public class SeatBookingDetailController {
	@Autowired
	private SeatBookingDetailService seatBookingDetailService;
	
	
	@RequestMapping(value="/getseatbookingstatus",method=RequestMethod.GET)
	public ResponseEntity<?> getseatbookingstatus(){
		try {

			return new ResponseEntity<>(seatBookingDetailService.list(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	
	@RequestMapping(value="/gettotalavailablesheetofac/{trainId}/{bookingDate}",method=RequestMethod.GET)
	public ResponseEntity<?> getTotalAvailableSheetOfAc(@PathVariable("trainId") int trainId, @PathVariable("bookingDate") Date bookingDate){
		try {
		System.out.println(bookingDate);
		
		

			return new ResponseEntity<>(seatBookingDetailService.getTotalAvailableSheetOfAc(trainId,bookingDate),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	@RequestMapping(value="/gettotalavailablesheetofsleeper/{trainId}/{bookingDate}",method=RequestMethod.GET)
	public ResponseEntity<?> getTotalAvailableSheetOfSleeper(@PathVariable("trainId") int trainId, @PathVariable("bookingDate") Date bookingDate){
		try {

			return new ResponseEntity<>(seatBookingDetailService.getTotalAvailableSheetOfSleeper(trainId,bookingDate),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	
	
	@RequestMapping(value="/gettotalsheetofbirthpreference/{birthName}/{trainId}/{bookingDate}",method=RequestMethod.GET)
	public ResponseEntity<?> getTotalSheetOfBirthPreference(@PathVariable("birthName") String birthName, @PathVariable("trainId") int trainId ,@PathVariable("bookingDate") Date bookingDate){
		try {

			return new ResponseEntity<>(seatBookingDetailService.getTotalSheetOfBirthPreference(birthName ,trainId,bookingDate),HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}
	
	
	
	
	@RequestMapping(value="/getstatusofberthid/{sbId}",method=RequestMethod.GET)
	public ResponseEntity<?> getStatusOfBerthId(@PathVariable("sbId") int sbId){
		try {

			return new ResponseEntity<>(seatBookingDetailService.getStatusOfBerthId(sbId),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	
	@RequestMapping(value="/statusofberthid",method=RequestMethod.PUT)
	public ResponseEntity<?> statusOfBerthId(@RequestBody SeatBookingDetail seatBookingDetail){
		try {

			seatBookingDetailService.postStatusOfBerthId(seatBookingDetail);
			return new ResponseEntity<>(HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}
	
	
	

	@RequestMapping(value="/getstatusofavailableberth/{trainId}/{coachId}/{bookingDate}",method=RequestMethod.GET)
	public ResponseEntity<?> getStatusOfAvailableBerth(@PathVariable("trainId") int trainId, @PathVariable("coachId") int coachId, @PathVariable("bookingDate") Date bookingDate){
		try {

			return new ResponseEntity<>(seatBookingDetailService.getStatusOfAvailableBerth(trainId,coachId,bookingDate),HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}
	
	@RequestMapping(value="/changestatusofberthid",method=RequestMethod.PUT)
	public ResponseEntity<?> changeStatusOfBerthId(@RequestBody SeatBookingDetail seatBookingDetail){
		try {

			seatBookingDetailService.postStatusOfBerthId(seatBookingDetail);
			return new ResponseEntity<>(HttpStatus.OK);

		} 
		catch (Exception e) {

			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);

		}
	}
	
	
	
}
