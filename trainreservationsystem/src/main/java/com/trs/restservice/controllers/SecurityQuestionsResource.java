package com.trs.restservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.SecurityQuestionService;

@RestController
@CrossOrigin(origins="*")
public class SecurityQuestionsResource {

	@Autowired
	private SecurityQuestionService securityQuestionService;
	
	@RequestMapping(value="/getquestions",method=RequestMethod.GET)
	public ResponseEntity<?> getQuestions(){
		
		try {
			
			return new ResponseEntity<>(securityQuestionService.list(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	@RequestMapping(value="/getactivequestions",method=RequestMethod.GET)
	public ResponseEntity<?> getActiveQuestions(){
		
		try {
			
			return new ResponseEntity<>(securityQuestionService.listOfActiveSecurityQuestions(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
}