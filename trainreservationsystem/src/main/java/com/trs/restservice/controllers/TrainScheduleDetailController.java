package com.trs.restservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trs.restservice.models.errormessage.ErrorMessage;
import com.trs.restservice.service.TrainScheduleDetailService;


@RestController
@CrossOrigin(origins="*")
public class TrainScheduleDetailController {

	@Autowired
	private TrainScheduleDetailService  trainScheduleDetailService;
	
	@RequestMapping(value="/getstation",method=RequestMethod.GET)
	public ResponseEntity<?> getstation(){
		try {

			return new ResponseEntity<>(trainScheduleDetailService.list(),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	
	
	@RequestMapping(value="/gettrainbytrainid/{trainId}",method=RequestMethod.GET)
	public ResponseEntity<?> getTrainByTrainId(@PathVariable("trainId") int trainId){
		try {

			return new ResponseEntity<>(trainScheduleDetailService.listByTrainId(trainId),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
	
	
	@RequestMapping(value="/gettrainbystation/{stationCode}",method=RequestMethod.GET)
	public ResponseEntity<?> getTrainByyStation(@PathVariable("stationCode") String stationCode){
		try {

			return new ResponseEntity<>(trainScheduleDetailService.listByStation(stationCode),HttpStatus.OK);
		} 
		catch (Exception e) {
			String errorMessage=e+" ---------- Error";
			ErrorMessage error=new ErrorMessage();
			error.setMessage(errorMessage);
			error.setStatus(HttpStatus.BAD_REQUEST);
			return new ResponseEntity<ErrorMessage>(error,HttpStatus.BAD_REQUEST);
		}

	}
}
