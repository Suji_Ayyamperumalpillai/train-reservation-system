package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.TrainDetailDao;
import com.trs.restservice.models.TrainDetail;

@Service
@Transactional
public class TrainDetailService {

	@Autowired
	private TrainDetailDao trainDetailDao;
	
	public List<TrainDetail> list() {	
		return trainDetailDao.list();
	}

	public TrainDetail listByTrainId(int trainId) {
		return trainDetailDao.listByTrainId(trainId);
	}

}
