package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.AlertAndUpdateDetailDao;
import com.trs.restservice.models.AlertAndUpdateDetail;

@Service("alertAndUpdateDetailService")
@Transactional
public class AlertAndUpdateDetailService {

	@Autowired
	private AlertAndUpdateDetailDao alertAndUpdateDetailDao;

	public void saveAlertsDetails(AlertAndUpdateDetail alertAndUpdateDetail){
		alertAndUpdateDetailDao.saveAlertsDetails(alertAndUpdateDetail);
	}

	public AlertAndUpdateDetail getAlertsDetailDao(int audId){
		return alertAndUpdateDetailDao.getAlertsDetailDao(audId);
	}

	public List<AlertAndUpdateDetail> list(){
		return alertAndUpdateDetailDao.list();
	}
	
	public List<AlertAndUpdateDetail> listOfActiveUpdates(){
		return alertAndUpdateDetailDao.listOfActiveUpdates();
	}
}
