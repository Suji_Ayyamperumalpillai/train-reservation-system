package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.UserAddressDetailDao;
import com.trs.restservice.models.UserAddressDetail;

@Service
@Transactional
public class UserAddressDetailService {

	@Autowired
	private UserAddressDetailDao addressDetailDao;
	
	public void saveUserAddress(UserAddressDetail addressDetail){
		addressDetailDao.saveUserAddress(addressDetail);
	}
	
	public void updateUserAddress(UserAddressDetail addressDetail){
		addressDetailDao.updateUserAddress(addressDetail);
	}
	public void deleteUserAddress(int uadId){
		addressDetailDao.deleteUserAddress(uadId);
	}
	
	public UserAddressDetail getUserAddress(int uadId){
		return addressDetailDao.getUserAddress(uadId);
	}
	
	public List<UserAddressDetail> list(){
		return addressDetailDao.list();
	}
	
}
