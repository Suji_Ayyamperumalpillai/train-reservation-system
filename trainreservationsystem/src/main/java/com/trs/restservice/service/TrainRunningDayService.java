package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.TrainRunningDayDao;
import com.trs.restservice.models.City;
import com.trs.restservice.models.TrainRunningDay;

@Service
@Transactional
public class TrainRunningDayService {
	
	@Autowired
	TrainRunningDayDao trainRunningDayDao;

	public List<TrainRunningDay> list(int trainId) {
		return trainRunningDayDao.list(trainId);
	}

}
