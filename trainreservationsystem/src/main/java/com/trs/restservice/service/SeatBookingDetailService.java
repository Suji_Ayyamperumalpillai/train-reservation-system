package com.trs.restservice.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.SeatBookingDetailDao;
import com.trs.restservice.models.BerthDetail;
import com.trs.restservice.models.SeatBookingDetail;
import com.trs.restservice.models.TrainScheduleDetail;

@Service
@Transactional
public class SeatBookingDetailService {
	@Autowired
	private SeatBookingDetailDao  seatBookingDetailDao;

	public List<SeatBookingDetail> list() {
		return seatBookingDetailDao.list();
	}

	
	public Long getTotalAvailableSheetOfAc(int trainId, Date bookingDate) {

		return seatBookingDetailDao.getTotalAvailableSheetOfAc(trainId,bookingDate);
	}

	public int getTotalAvailableSheetOfSleeper(int trainId, Date bookingDate) {
		return seatBookingDetailDao.getTotalAvailableSheetOfSleeper(trainId,bookingDate);
	}
	
	public List<SeatBookingDetail> getTotalSheetOfBirthPreference(String birthName, int trainId, Date bookingDate) {

		return seatBookingDetailDao.getTotalSheetOfBirthPreference(birthName,trainId,bookingDate);
	}
	
	

	public List<SeatBookingDetail> getStatusOfBerthId(int sbId) {

		return seatBookingDetailDao.getStatusOfBerthId(sbId);
	}

	public void postStatusOfBerthId(SeatBookingDetail seatBookingDetail) {
		seatBookingDetailDao.postStatusOfBerthId(seatBookingDetail);

	}

	public List<SeatBookingDetail> getStatusOfAvailableBerth(int trainId, int coachId, Date bookingDate) {

		return seatBookingDetailDao.getStatusOfAvailableBerth(trainId,coachId,bookingDate);
	}


	
}
