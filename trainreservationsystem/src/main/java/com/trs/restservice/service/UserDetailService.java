package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.UserDetailDao;
import com.trs.restservice.models.UserDetail;

@Service
@Transactional
public class UserDetailService {

	@Autowired
	private UserDetailDao userDetailDao;

	public void saveUserDetails(UserDetail userDetail){
		userDetailDao.saveUserDetails(userDetail);
	}

	public void updateUserDetail(UserDetail userDetail){
		userDetailDao.updateUserDetail(userDetail);
	}

	public void deleteUserDetail(int udId){
		userDetailDao.deleteUserDetail(udId);
	}

	public UserDetail getUserDetail(int udId)
	{
		return userDetailDao.getUserDetail(udId);
	}

	public List<UserDetail> getList(){
		return userDetailDao.getList();
	}
	public UserDetail getUserById(String userId) {
		return userDetailDao.getUserByUserId(userId);
	}

	public boolean isAuthUser(UserDetail user) {
		return userDetailDao.isAuthUser(user);
	}
	
	public boolean isUserExist(UserDetail user) {
		return userDetailDao.isUserExist(user);
	}
}
