package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.CityDao;
import com.trs.restservice.models.City;

@Service
@Transactional
public class CityService {

	@Autowired
	private CityDao cityDao;

	public City getCity(int cityId){
		return cityDao.getCity(cityId);
	}
	public List<City> list(){
		return cityDao.list();
	}
	public List<City> listByStateId(int stateId){
		return cityDao.listByStateId(stateId);
	}
}
