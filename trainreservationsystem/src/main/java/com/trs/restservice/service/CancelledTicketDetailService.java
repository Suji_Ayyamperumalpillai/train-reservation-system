package com.trs.restservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.CancelledTicketDetailDao;
import com.trs.restservice.models.CancelledTicketDetail;

@Service
@Transactional
public class CancelledTicketDetailService {

	@Autowired
	CancelledTicketDetailDao  cancelledTicketDetailDao;

	public void saveCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail) {
		cancelledTicketDetailDao.saveCancelledTicketDetail(cancelledTicketDetail);
	}
	
	public CancelledTicketDetail getCancelledTicketDetail(CancelledTicketDetail cancelledTicketDetail){
		return cancelledTicketDetailDao.getCancelledTicketDetail(cancelledTicketDetail);
	}
}
