package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.TransactionDetailDao;
import com.trs.restservice.dao.UserDetailDao;
import com.trs.restservice.models.TransactionDetail;

@Service
@Transactional
public class TransactionDetailService {

	@Autowired
	private TransactionDetailDao transactionDetailDao;

	public List<TransactionDetail> getTransactionDetailByUserId(int udId) {

		return transactionDetailDao.getTransactionDetailByUserId(udId) ;
	}

	public void changeReservationStatus(TransactionDetail transactionDetail) {
		transactionDetailDao.changeReservationStatus(transactionDetail);

	}


	public void saveTransactionDetail(TransactionDetail transactionDetail){
		transactionDetailDao.saveTransactionDetail(transactionDetail);
	}

	public TransactionDetail getLastTransactionDetailByUserId(int udId) {
		return transactionDetailDao.getLastTransactionDetailByUserId(udId);

	}


	//ADDED BY SUJI
	public List<TransactionDetail> getBookedTicketsByUserId(int udId) {

		return transactionDetailDao.getBookedTicketsDetailByUserId(udId);
	}
}
