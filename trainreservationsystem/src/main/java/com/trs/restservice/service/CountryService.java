package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.CountryDao;
import com.trs.restservice.models.Country;

@Service
@Transactional
public class CountryService {

	@Autowired
	private CountryDao countryDao;

	public Country getCountry(int countryId){
		return countryDao.getCountry(countryId);
	}

	public List<Country> list(){
		return countryDao.list();
	}
}
