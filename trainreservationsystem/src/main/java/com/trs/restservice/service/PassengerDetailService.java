package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.trs.restservice.dao.PassengerDetailDao;
import com.trs.restservice.models.PassengerDetail;
import com.trs.restservice.models.TransactionDetail;

@Service
@Transactional
public class PassengerDetailService {
	
	
	@Autowired
	private PassengerDetailDao passengerDetailDao;

	public void savePassengerDetails(PassengerDetail passengerDetail) {
		passengerDetailDao.savePassengerDetails(passengerDetail);
		
	}

	public PassengerDetail getPassengerByTransactionId(int tnsId) {
		return passengerDetailDao.getPassengerByTransactionId(tnsId);
	}

	public List<PassengerDetail> getPessengerDetails() {
		
		return passengerDetailDao.getPessengerDetails();
	}

}
