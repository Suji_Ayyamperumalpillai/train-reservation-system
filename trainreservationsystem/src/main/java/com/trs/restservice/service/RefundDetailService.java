package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.RefundDetailDao;
import com.trs.restservice.models.RefundDetail;

@Service
@Transactional
public class RefundDetailService {

	@Autowired
	RefundDetailDao  refundDetailDao;

	public List<RefundDetail> getRefundDetails(int udId) {
		return refundDetailDao.getRefundDetailList(udId);

	}

	public void saveRefundDetails(RefundDetail refundDetail) {
		refundDetailDao.saveRefundDetail(refundDetail);

	}

}
