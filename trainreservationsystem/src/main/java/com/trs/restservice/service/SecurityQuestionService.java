package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.SecurityQuestionDao;
import com.trs.restservice.models.SecurityQuestion;

@Service("securityQuestionService")
@Transactional
public class SecurityQuestionService {

	@Autowired
	private SecurityQuestionDao securityQuestionDao;
	
	public SecurityQuestion getSecurityQuestion(int sqId){
		return securityQuestionDao.getSecurityQuestion(sqId);
	}

	public List<SecurityQuestion> list(){
		return securityQuestionDao.list();
	}

	public List<SecurityQuestion> listOfActiveSecurityQuestions(){
		return securityQuestionDao.listOfActiveSecurityQuestions();
	}
}
