package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.StateDao;
import com.trs.restservice.models.State;

@Service
@Transactional
public class StateService {

	@Autowired
	private StateDao stateDao;

	public State getCountry(int stateId){
		return stateDao.getCountry(stateId);
	}

	public List<State> list(){
		return stateDao.list();
	}
	
	public List<State> listBycountryId(int countryId){
		return stateDao.listByCountryId(countryId);
	}
}
