package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.TrsContactDetailDao;
import com.trs.restservice.models.TrsContactDetail;

@Service
@Transactional
public class TrsContactDetailService {

	@Autowired
	private TrsContactDetailDao trsContactDetailDao;

	public void saveContactsDetails(TrsContactDetail trsContactDetail ){
		trsContactDetailDao.saveContactsDetails(trsContactDetail);
	}

	public TrsContactDetail getTrsContactDetailDao(int trscId){
		return trsContactDetailDao.getTrsContactDetailDao(trscId);
	}

	public List<TrsContactDetail> list(){
		return trsContactDetailDao.list();
	}
}
