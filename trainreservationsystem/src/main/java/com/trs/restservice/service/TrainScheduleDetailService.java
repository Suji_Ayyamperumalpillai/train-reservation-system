package com.trs.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trs.restservice.dao.TrainScheduleDetailDao;
import com.trs.restservice.models.TrainScheduleDetail;
import com.trs.restservice.models.TrsContactDetail;

@Service
@Transactional
public class TrainScheduleDetailService {
	
	@Autowired
	private TrainScheduleDetailDao  trainScheduleDetailDao;

	public List<TrainScheduleDetail> list() {
		return trainScheduleDetailDao.list();
	}

	public List<TrainScheduleDetail> listByStation(String stationCode) {
		return trainScheduleDetailDao.listByStation(stationCode);
	}

	public List<TrainScheduleDetail> listByTrainId(int trainId) {
		
		return trainScheduleDetailDao.listByTrainId(trainId);
	}
	
}
