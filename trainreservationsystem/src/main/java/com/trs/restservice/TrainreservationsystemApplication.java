package com.trs.restservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainreservationsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainreservationsystemApplication.class, args);
	}
}
