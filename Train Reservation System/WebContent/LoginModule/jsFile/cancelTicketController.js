app.controller('cancelTicketController',function($scope,$rootScope,$http,$location){
	console.log("inside cancelTicketController");
	console.log($rootScope.currentUser);
	console.log($rootScope.currentUser.udId);
	
	var getTransactionHistory=function(){
		var res = $http.get('http://localhost:8080/getbookedtickets/'+$rootScope.currentUser.udId);
		res.then(function(response){
			$scope.transactions = response.data;
			console.log(response.data);
			console.log($scope.transactions)
			console.log("status:" + response.status);
		});
	}
	getTransactionHistory();
	
	$scope.cancelTicket=function(){
		console.log("inside cancel ticket");
		console.log($scope.selectTicket);
		
		//make status in transaction details as cancelled
		var transactionObj=$scope.selectTicket;
		transactionObj.reservationStatus="CANCELLED";
		console.log(transactionObj);
		
		var res = $http.put('http://localhost:8080/changereservationstatus',transactionObj);
		res.then(function mySuccess(response){
			console.log("in success");
			saveCancelledTicket();
		}
		, function myError(response) {	
			console.log("in error");
			//write error code
		});
		//add in cancelled data table
		var saveCancelledTicket=function(){
			//calculate refund amount
			//refunded amount is amount deducted by 5%
			var refundAmount=(transactionObj.transactionAmount)*0.95;
			
			var cancelTicketObject={
					transactionDetail:{
						tnsId:transactionObj.tnsId
					},
					refundAmount:refundAmount,
					dateOfCancellation: new Date()//today's date
			}
			
			console.log(cancelTicketObject);
			
			var res = $http.post('http://localhost:8080/cancelticket',cancelTicketObject);
			res.then(function mySuccess(response){
				console.log("in success");
				var cancelTicketObject1=response.data;
				console.log(cancelTicketObject1);
				saveRefundTicket(cancelTicketObject1);
			}
			, function myError(response) {	
				console.log("in error");
				//write error code
			});
			
			
		}
			
		
		//add in refund data table
		
		var saveRefundTicket=function(cancelTicketObject){
			var refundObject={
					
			cancelledTicketDetail:{
				ctId:cancelTicketObject.ctId,
				transactionDetail:{
					tnsId:transactionObj.tnsId
				}
			},
			refundStatus: 'SUCCESS',
			bank: 'ICICI',
			refundDate: new Date()
			}
			
			console.log(refundObject);
			
			var res = $http.post('http://localhost:8080/addrefunddetails',refundObject);
			res.then(function mySuccess(response){
				console.log("in success");
				getTransactionHistory();
				
			}
			, function myError(response) {	
				console.log("in error");
				//write error code
			});
		}
	}
	
});