app.controller('signUpController',function($scope,$http,$location){
	console.log("inside signUp password controller");
	$scope.countries=[];

	//captcha code generation
	//code
	var captchaCode = "";


	getCaptcha = function(){
		var a = Math.ceil(Math.random() * 9)+ '';
		var b = Math.ceil(Math.random() * 9)+ '';       
		var c = Math.ceil(Math.random() * 9)+ '';  
		var d = Math.ceil(Math.random() * 9)+ '';  
		var e = Math.ceil(Math.random() * 9)+ '';  
		var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e;
		document.getElementById("txtCaptcha").value = code

		captchaCode=code;
		console.log(captchaCode);
		//removing spaces from captcha code
		captchaCode = captchaCode.replace(/\s/g, '');
		console.log(captchaCode);
	}
	getCaptcha();

	$scope.checkUserExistance=function(keyEvent) {
		//var disableSubmit;
		console.log($scope.userId);
		var userObject={
				userId: $scope.userId
		}
		var res = $http.post('http://localhost:8080/isuserexist',userObject);
		res.then(function mySuccess(response){
			console.log("in success");
			$scope.userExistance=response.data.message;
			$scope.submit = "";
			$scope.disableSubmit=true;
		}
		, function myError(response) {	
			console.log("in error");
			$scope.userExistance='';
			$scope.submit = "true";
			$scope.disableSubmit=false;
			//write error code
		});
		console.log("finally = "+$scope.disableSubmit);
	}

	var getSecurityQuestions=function(){
		var res = $http.get('http://localhost:8080/getactivequestions');
		res.then(function(response){
			$scope.securityQuestions = response.data;
			console.log($scope.securityQuestions );
			console.log("status:" + response.status);
		});

	}
	getSecurityQuestions();
	console.log($scope.securityQuestion);

	var getCountry=function(){
		var res = $http.get('http://localhost:8080/getcountries');
		res.then(function(response){
			$scope.countries = response.data;
			console.log($scope.countries );
			console.log("status:" + response.status);
		});

	}
	getCountry();

	$scope.getState=function(){
		countryId=$scope.selectedCountry.countryId;
		console.log(countryId);
		var res = $http.get('http://localhost:8080/getstates/'+countryId);
		res.then(function(response){
			$scope.stateDetails = response.data;
			console.log(response.data);
			console.log("status:" + response.status);
		});

	}

	$scope.getCity=function(){
		stateId=$scope.selectedStateId;
		console.log($scope.selectedStateId);
		console.log(stateId);
		var res = $http.get('http://localhost:8080/getcities/'+stateId);
		res.then(function(response){
			$scope.cityDetails = response.data;
			console.log(response.data);
			console.log("status:" + response.status);
		}); 
	}

	//save user and address function
	$scope.saveUser=function(){
		console.log($scope.userPassword);
		console.log($scope.confirmUserPassword);
		console.log(captchaCode);
		console.log($scope.enteredCptchCode);


		if(captchaCode != $scope.enteredCptchCode){
			alert("invalid Captcha");
		}
		else{
			console.log("success save user");		
			if($scope.userPassword===$scope.confirmUserPassword){
				var userObject={
						firstName:$scope.firstName,
						lastName:$scope.lastName,
						userId:$scope.userId,
						userPassword:$scope.userPassword,
						gender:$scope.gender,
						dateOfBirth:$scope.dateOfBirth,
						emailId:$scope.emailId,
						mobileNumber:$scope.mobileNumber,
						nationality:$scope.nationality.countryName,
						securityQuestion:$scope.securityQuestion.securityQuestion,
						securityAnswer:$scope.securityAnswer

				}
				console.log(userObject);
				var udId;
				//posting data 			
				var res=$http.post('http://localhost:8080/adduser', userObject)
				res.then(function(response) {
					console.log(response.data);
					udId=response.data.udId;
					console.log(udId);
					saveUserAddress(udId);
					//$location.path('/registrationSuccessful');
				});
				var saveUserAddress= function(udId){
					var userAddress={
							userDetail:{ udId: udId },
							area:$scope.completeAdd,
							pincode:$scope.pincode,
							city: { cityId:$scope.selectedCity}					
					}
					console.log(userAddress);
					var res1=$http.post('http://localhost:8080/adduseraddress', userAddress)
					res1.then(function(response) {
						//$scope.registrationMessage="You have successfully registered!";	
						//console.log(response.data);
						$location.path('/registrationSuccessful');
					});
				}
				//these are deprecated
				/*var res = $http.post('http://localhost:8080/adduser', userObject);
			res.success(function(data, status, headers, config) {
				$scope.successMessage="successfully registered";
				$location.path('/registrationSuccessful');

			});*/
				/*res.error(function(data, status, headers, config) {
				alert( "failure message: " + JSON.stringify(userObject));
				console.log("in error");
				$scope.message=data.message;
			});*/	
			}
			else{
				alert("re-entered password does not matches with password!!!")
				$scope.message="ENTER password again";
			}
		}

	}

});