var app = angular.module('myApp1', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {


		$urlRouterProvider.otherwise('/mhome/home');

		$stateProvider

		// HOME STATES AND NESTED VIEWS ========================================
		.state('mhome', {
			url: '/mhome',
			templateUrl: 'mainHome.html'
		})
		
		// nested list with custom controller
		.state('mhome.home', {
			url: '/home',
			templateUrl: 'homePage.html',
			controller: 'loginController'
		})
		
		.state('mhome.forgetPassword', {
			url: '/forgetPassword',
			templateUrl: 'forgetPassword.html',
			controller: 'forgetPasswordController'
		})
		
		.state('mhome.signUp', {
			//url: '^/signUp', is used for absolute routing so its url is
			//        /signUp and not /mhome/signUp
	        url: '^/signUp',
	        templateUrl: 'signUp.html',
	        controller: 'signUpController'
	    })
	    
	    .state('mhome.passwordSuccessfullyChanged', {
	    	url:'^/passwordSuccessfullyChanged',
			templateUrl : "passwordSuccessfullyChanged.html"
		})
		.state('mhome.registrationSuccessful', {
	    	url:'^/registrationSuccessful',
			templateUrl : "registrationSuccessful.html"
		})
		
		.state('trsContact', {
	    	url:'/trsContact',
			templateUrl : "trsContactHome.html",
			controller:'trsContactController'
		})
		
		// ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
		.state('userHome', {
			url: '/userHome',
			templateUrl: '../../UserModule/htmlfiles/userHome.html',
			controller : 'userHomeController'
		})
		.state('userHome.userService', {
			url: '/userService',
			templateUrl: '../../UserModule/htmlfiles/userService.html',
			controller : 'userServiceController'
		})
		
		
		
		/*.state('userHome.userService.lastTransaction', {
			url: '/lastTransaction',
			templateUrl: '../../UserModule/htmlfiles/lastTransaction.html',
			controller : 'userHomeController'
		})*/
		.state('userHome.trainDetails', {
			url: '/trainDetails',
			templateUrl: '../../UserModule/htmlfiles/trainDetails.html'
			, controller : 'userHomeController'
		})
		
		.state('userHome.trainDetails.seatBooking', {
			url: '/seatBooking',
			templateUrl: '../../UserModule/htmlfiles/seatBookingDetails.html'
			, controller : 'userHomeController'
		})
		
		.state('userHome.bookedTicketHistory', {
			url: '/bookedTicketHistory',
			templateUrl: '../../UserModule/htmlfiles/bookedTicketHistory.html'
			, controller : 'ticketHistoryController'
		})
		.state('userHome.refundDetail', {
			url: '/refundDetail',
			templateUrl: '../../UserModule/htmlfiles/refundDetail.html'
			, controller : 'refundDetailController'
		})
		.state('userHome.cancelTicket', {
			url: '/cancelTicket',
			templateUrl: '../../UserModule/htmlfiles/cancelTicket.html',
			controller : 'cancelTicketController'
		});

	});


