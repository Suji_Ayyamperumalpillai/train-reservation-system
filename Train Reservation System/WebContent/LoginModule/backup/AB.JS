var app = angular.module("myApp1", ["ngRoute"]);
app.config(['$routeProvider',function($routeProvider) {
	$routeProvider
	.when("/", {
		templateUrl : "login.html",
		controller : 'loginController'
	})
	.when("/forgetPassword", {
		templateUrl : "forgetPassword.html",
		controller:'forgetPasswordController'
	})
	.when("/trsContact", {
		templateUrl : "trsContactHome.html",
		controller:'trsContactController'
	})
	.when("/signUp", {
		templateUrl : "signUp.html",
		controller:'signUpController'
	}); 
}]);


app.controller('loginController',function($scope,$http,$location){
	console.log("inside login controller");

	var captchaCode = "";

	getCaptcha = function(){
		var a = Math.ceil(Math.random() * 9)+ '';
		var b = Math.ceil(Math.random() * 9)+ '';       
		var c = Math.ceil(Math.random() * 9)+ '';  
		var d = Math.ceil(Math.random() * 9)+ '';  
		var e = Math.ceil(Math.random() * 9)+ '';  
		var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e;
		document.getElementById("txtCaptcha").value = code

		captchaCode=code;
		console.log(captchaCode);
	}


	var getAlertsAndUpdate=function(){
		var res = $http.get('http://localhost:8080/getactivealerts');
		res.then(function(response){
			$scope.alertsAndUpdateDetails = response.data;
			console.log(response.data);
			console.log("status:" + response.status);
		});
	}

	getCaptcha();
	getAlertsAndUpdate();
	console.log($scope.displayedCaptchaCode);



	$scope.loginUser=function(){

		if(captchaCode != $scope.enteredCptchCode){
			location.reload();
			alert("invalid Captcha");
		}
		else{

			var userObject={
					user_id:$scope.userId,
					user_password:$scope.userPwd
			}
			var res = $http.get('http://localhost:8080/authuser',userObject);
			res.then(function(response){
				$scope.user = response.data;
				console.log(response.data);
				console.log("status:" + response.status);
				$location.path('/index')
			});
			/*res.error(function(data, status, headers, config) {
					$scope.errorMessage="wrong user name password.";
					console.log("in error");
					$location.path('/index');
				}); */

		}

	}


});

app.controller('forgetPasswordController',function($scope,$http,$location){
	console.log("inside forget password controller");
	$scope.basicUserdetails=true;
	$scope.userdetails=false;

	var captchaCode = "";

	getCaptcha = function(){
		var a = Math.ceil(Math.random() * 9)+ '';
		var b = Math.ceil(Math.random() * 9)+ '';       
		var c = Math.ceil(Math.random() * 9)+ '';  
		var d = Math.ceil(Math.random() * 9)+ '';  
		var e = Math.ceil(Math.random() * 9)+ '';  
		var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e;
		document.getElementById("txtCaptcha").value = code

		captchaCode=code;
		console.log(captchaCode);
	}

	getCaptcha();

	$scope.fetchUserDetails=function(){
		
		if(captchaCode != $scope.enteredCptchCode){
			alert("invalid Captcha");
		}
		else{
			$scope.basicUserdetails=false;
			$scope.userdetails=true;
			var res = $http.get('http://localhost:8080/getuser/'+userId);
			res.then(function(response){
				$scope.userDetails = response.data;
				console.log(response.data);
				console.log("status:" + response.status);
			})
			res.error(function(data, status, headers, config) {
				alert( "failure message: " + JSON.stringify({data: data}));
			}); 
		}
	}



	$scope.changePassword=function(){
		$scope.basicUserdetails=false;
		$scope.userdetails=true;
		if(captchaCode != $scope.enteredCptchCode){
			alert("invalid Captcha");
		}
		else{
			if($scope.userPwd===$scope.userCnfPwd){
				var userPwdDetails={
						user_id:$scope.userId,
						security_question:$scope.securityQues,
						security_answer:$scope.securityAns,
						user_password:$scope.userPwd,
				}

				var res = $http.post('http://localhost:8080/updateuser', userPwdDetails);
				res.success(function(data, status, headers, config) {
					$scope.successMessage="Password Successfully Changed";
					$location.path('/passwordSuccessfullyChanged.html');

				});
				res.error(function(data, status, headers, config) {
					alert( "failure message: " + JSON.stringify(userObject));
					console.log("in error");
					$scope.message=data.message;
				});	
			}
			else{
				alert("re-entered password does not matches with password!!!")
				$scope.message="ENTER password again";
			}
		}

	}
});

app.controller('signUpController',function($scope,$http,$location){
	console.log("inside signUp password controller");
	$scope.countries=[];

	var captchaCode = "";

	getCaptcha = function(){
		var a = Math.ceil(Math.random() * 9)+ '';
		var b = Math.ceil(Math.random() * 9)+ '';       
		var c = Math.ceil(Math.random() * 9)+ '';  
		var d = Math.ceil(Math.random() * 9)+ '';  
		var e = Math.ceil(Math.random() * 9)+ '';  
		var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e;
		document.getElementById("txtCaptcha").value = code

		captchaCode=code;
		console.log(captchaCode);
	}

	getCaptcha();


	var getSecurityQuestions=function(){
		var res = $http.get('http://localhost:8080/getactivequestions');
		res.then(function(response){
			$scope.securityQuestions = response.data;
			console.log($scope.securityQuestions );
			console.log("status:" + response.status);
		});

	}
	getSecurityQuestions();
	console.log($scope.securityQuestion);

	var getCountry=function(){
		var res = $http.get('http://localhost:8080/getcountries');
		res.then(function(response){
			$scope.countries = response.data;
			console.log($scope.countries );
			console.log("status:" + response.status);
		});

	}

	//checkUserExistance();

	getCountry();

	$scope.getState=function(){
		countryId=$scope.selectedCountry.countryId;
		console.log(countryId);
		var res = $http.get('http://localhost:8080/getstates/'+countryId);
		res.then(function(response){
			$scope.stateDetails = response.data;
			console.log(response.data);
			console.log("status:" + response.status);
		});

	}

	$scope.getCity=function(){
		stateId=$scope.selectedStateId;
		console.log($scope.selectedStateId);
		console.log(stateId);
		var res = $http.get('http://localhost:8080/getcities/'+stateId);
		res.then(function(response){
			$scope.cityDetails = response.data;
			console.log(response.data);
			console.log("status:" + response.status);
		}); 
	}


	$scope.ValidateUser=function(keyEvent) {

		var res = $http.get('http://localhost:8080/getuser');
		res.then(function(response){
			$scope.userDetails = response.data;
			console.log(response.data);

			for(i=o; i<=$scope.userDetails.length; i++){
				if($scope.userDetails.user_id == $scope.userId){
					$scope.userExistance = "user already exist"
				}	
			}

			console.log("status:" + response.status);
		})
		/*res.error(function(data, status, headers, config) {
			alert( "failure message: " + JSON.stringify({data: data}));
		}); */

	}

	$scope.saveUser=function(){
		if(captchaCode != $scope.enteredCptchCode){
			alert("invalid Captcha");
		}
		else{
			if($scope.userPassword===$scope.confirmUserPassword){
				var userObject={
						first_name:$scope.firstName,
						last_name:$scope.lastName,
						user_id:$scope.userId,
						user_password:$scope.userPassword,
						gender:$scope.gender,
						date_of_birth:$scope.dateOfBirth,
						email_id:$scope.emailId,
						mobile_number:$scope.mobileNumber,
						nationality:$scope.nationality,
						security_question:$scope.securityQuestion,
						security_answer:$scope.securityAnswer,
						area:$scope.completeAdd,
						pincode:$scope.pinCode,
						city_id:$scope.city,
						state_id:$scope.state,
						country_id:$scope.country
				}
				console.log(userObject);
				console.log($scope.phonenumber);
				//posting data for checking his authorization
				var res = $http.post('http://localhost:8080/adduser', userObject);
				res.success(function(data, status, headers, config) {
					$scope.successMessage="successfully registered";
					$location.path('/registrationSuccessful');

				});
				/*res.error(function(data, status, headers, config) {
				alert( "failure message: " + JSON.stringify(userObject));
				console.log("in error");
				$scope.message=data.message;
			});*/	
			}
			else{
				alert("re-entered password does not matches with password!!!")
				$scope.message="ENTER password again";
			}
		}
	}

});


app.controller('trsContactController',function($scope,$http,$location){
	console.log("inside trs contact controller");
	$scope.getTrsContactInfo=function(){

		var res = $http.get('http://localhost:8080/getcontacts');
		res.then(function(response){
			$scope.trsContactDetails = response.data;
			console.log(response.data);
			console.log("status:" + response.status);
		}); 
	}
});